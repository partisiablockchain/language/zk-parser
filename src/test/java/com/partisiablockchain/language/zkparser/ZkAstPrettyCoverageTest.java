package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test the RustZk pretty-printer. */
public final class ZkAstPrettyCoverageTest {

  private static final ZkAst.Path u17_path = ZkAst.Path.simple(ZkAst.PosInterval.UNKNOWN, "u17");
  private static final ZkAst.Type u17 = new ZkAst.Type.Named(u17_path, ZkAst.PosInterval.UNKNOWN);

  private static final ZkAst.Path i31_path =
      ZkAst.Path.simple(ZkAst.PosInterval.UNKNOWN, "some_crate", "i31");
  private static final ZkAst.Type i31 = new ZkAst.Type.Named(i31_path, ZkAst.PosInterval.UNKNOWN);

  private static final ZkAst.Path vec_u17_path =
      new ZkAst.Path(List.of("std", "vec", "Vec"), List.of(u17), ZkAst.PosInterval.UNKNOWN);

  private static final ZkAst.Type u17_i31 =
      new ZkAst.Type.Tuple(List.of(u17, i31), ZkAst.PosInterval.UNKNOWN);

  @Test
  void prettyForTypesThatCannotBeConstructedFromRustCode() {
    final var ast =
        new ZkAst.Program(
            List.of(),
            List.of(
                new ZkAst.ProgramItem<ZkAst.ConstantDefinition>(
                    ZkAst.ItemVisibility.ONLY_FILE,
                    List.of(),
                    new ZkAst.ConstantDefinition(
                        "a",
                        u17_i31,
                        new ZkAst.Expression.TupleConstructor(
                            List.of(
                                new ZkAst.Expression.IntLiteral(
                                    new BigInteger("53"), null, ZkAst.PosInterval.UNKNOWN),
                                new ZkAst.Expression.IntLiteral(
                                    new BigInteger("-41"), null, ZkAst.PosInterval.UNKNOWN)),
                            ZkAst.PosInterval.UNKNOWN),
                        ZkAst.PosInterval.UNKNOWN),
                    ZkAst.PosInterval.UNKNOWN)),
            List.of(),
            List.of(),
            List.of(),
            List.of(),
            List.of());

    Assertions.assertThat(ZkAstPretty.pretty(ast))
        .isEqualTo("const a: (u17, some_crate::i31) = (53, -41);");
  }

  @Test
  void prettyForBlockExpr() {
    final var ast =
        new ZkAst.Program(
            List.of(
                new ZkAst.ProgramItem<>(
                    ZkAst.ItemVisibility.ONLY_FILE,
                    List.of(),
                    new ZkAst.Function(
                        "a",
                        List.of(),
                        ZkAst.Type.unit(ZkAst.PosInterval.UNKNOWN),
                        new ZkAst.Expression.IntLiteral(
                            BigInteger.ONE, null, ZkAst.PosInterval.UNKNOWN),
                        ZkAst.PosInterval.UNKNOWN),
                    ZkAst.PosInterval.UNKNOWN)),
            List.of(),
            List.of(),
            List.of(),
            List.of(),
            List.of(),
            List.of());

    final String expected =
        """
        fn a() {
            1
        }
        """;

    Assertions.assertThat(ZkAstPretty.pretty(ast)).isEqualTo(expected.strip());
  }

  @Test
  void prettyBuilderCoverage() {
    final var stringBuilder = new StringBuilder();
    final var builder = new ZkAstPretty.PrettyBuilder(stringBuilder, 1337, false);
    Assertions.assertThat(builder.output()).isEqualTo(stringBuilder);
    Assertions.assertThat(builder.indent()).isEqualTo(1337);
    Assertions.assertThat(builder.withinExpression()).isEqualTo(false);
  }

  @Test
  void prettyType() {
    Assertions.assertThat(ZkAstPretty.prettyType(u17_i31)).isEqualTo("(u17, some_crate::i31)");
  }

  @Test
  void prettyPath() {
    Assertions.assertThat(ZkAstPretty.prettyPath(u17_path, true)).isEqualTo("u17");
    Assertions.assertThat(ZkAstPretty.prettyPath(u17_path, false)).isEqualTo("u17");
    Assertions.assertThat(ZkAstPretty.prettyPath(i31_path, true)).isEqualTo("some_crate::i31");
    Assertions.assertThat(ZkAstPretty.prettyPath(i31_path, false)).isEqualTo("some_crate::i31");
    Assertions.assertThat(ZkAstPretty.prettyPath(vec_u17_path, false))
        .isEqualTo("std::vec::Vec<u17>");
    Assertions.assertThat(ZkAstPretty.prettyPath(vec_u17_path, true))
        .isEqualTo("std::vec::Vec::<u17>");
  }
}
