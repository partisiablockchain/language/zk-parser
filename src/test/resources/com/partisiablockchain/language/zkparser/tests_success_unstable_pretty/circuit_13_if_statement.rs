pub fn if_statement(x: i32) -> i32 {
    if x < 5 {
        return 0;
    } else if x < 10 {
        return 1;
    }

    if x < 15 {
        return 2;
    } else {
        return 3;
    }
}
