/**/
/***/
/*
*/
/*
Hello! :)
*/
/*   - Comment */
/**  - Outer block doc (exactly) 2 asterisks (not treated specially for our parser) */
/*** - Comment */

struct S {
    f1: /* Negative type */ i32,
    /* interesting field */ f2 /* Non-negative type */ : u32
}

fn ident(x: /* Type comment */ i32) -> i32 {
    /* hello /* nested /* comment */ */ */
    x
}