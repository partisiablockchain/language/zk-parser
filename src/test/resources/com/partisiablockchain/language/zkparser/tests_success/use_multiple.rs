use crate::pbc_zk::Sbi32;
use crate::pbc_zk::sbi32_from;

fn constant() -> Sbi32 {
    sbi32_from(99)
}
