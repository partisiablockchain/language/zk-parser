fn do_nothing(x: i32) -> i32 {
    ({} < x); x
}
