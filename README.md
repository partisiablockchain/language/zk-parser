# zk-parser

Parser for zero-knowledge Rust, which is a strict subset of Rust used to describe zero-knowledge
computations for the Partisia Blockchain.
