/// Perform a zk computation on secret-shared data.
/// Find the winner of the second-price auction with exactly 3 bidders.
/// Finds the highest bidder and the amount of the second-highest bid
pub fn zk_compute() -> (Sbi32, Sbi32) {
    let mut highest_bidder: Sbi32 = sbi32_from(0);
    let mut highest_amount: Sbi32 = sbi32_from(0);
    let mut second_highest_amount: Sbi32 = sbi32_from(0);
    if sbi32_input(0) > highest_amount {
        second_highest_amount = highest_amount;
        highest_amount = sbi32_input(0);
        highest_bidder = sbi32_metadata(0);
    } else if sbi32_input(0) > second_highest_amount {
        second_highest_amount = sbi32_input(0);
    }
    if sbi32_input(1) > highest_amount {
        second_highest_amount = highest_amount;
        highest_amount = sbi32_input(1);
        highest_bidder = sbi32_metadata(1);
    } else if sbi32_input(1) > second_highest_amount {
        second_highest_amount = sbi32_input(1);
    }
    if sbi32_input(2) > highest_amount {
        second_highest_amount = highest_amount;
        highest_amount = sbi32_input(2);
        highest_bidder = sbi32_metadata(2);
    } else if sbi32_input(2) > second_highest_amount {
        second_highest_amount = sbi32_input(2);
    }
    (highest_bidder, second_highest_amount)
}
