struct MyStruct {
    field1: u32,
    field2: u32,
}

fn func() -> MyStruct {
    let field1 = MyStruct { field1: 1, field2: 2 };
    let field2 = MyStruct { field1: 3, field2: 4 };
    MyStruct { field1: field1.field1, field2: field2.field2 }
}
