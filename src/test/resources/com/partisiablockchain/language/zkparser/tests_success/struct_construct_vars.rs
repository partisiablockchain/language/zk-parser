struct MyStruct {
    field1: u32,
    field2: u32,
}

fn func() -> MyStruct {
    let f1 = 1;
    let f2 = 2;
    MyStruct { field1: f1, field2: f2 }
}
