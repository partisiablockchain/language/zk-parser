struct Range<#[some_attribute] #[some_other_attribute] T: PartialOrd> {
    low: T,
    high: T,
}
