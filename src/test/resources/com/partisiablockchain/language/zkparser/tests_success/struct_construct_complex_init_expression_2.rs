struct MyStruct {
    field1: u32,
    field2: u32,
}

fn func() -> MyStruct {
    let v1 = 1;
    let v2 = 1;
    MyStruct { field1: v1 + v2, field2: v1 + v2 }
}
