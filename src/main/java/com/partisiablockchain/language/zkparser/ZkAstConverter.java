package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Map.entry;

import com.partisiablockchain.language.zkparser.parser.RustZkParser;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.Tree;

record ZkAstConverter(ArrayList<RustZk.ErrorLine> errorsAccum) {

  @SuppressWarnings("unchecked")
  static <T extends ZkAst.ProgramItemData> List<ZkAst.ProgramItem<T>> filterItems(
      List<ZkAst.ProgramItem<ZkAst.ProgramItemData>> allItems, Class<T> itemClass) {
    return allItems.stream()
        .filter(x -> x.item().getClass().equals(itemClass))
        .map(x -> (ZkAst.ProgramItem<T>) x)
        .toList();
  }

  static RustZk.ParseResult convertProgram(final RustZkParser.FileContext ctx) {
    final var converter = new ZkAstConverter(new ArrayList<>());
    final ZkAst.Program programAst = converter.convert(ctx);
    return new RustZk.ParseResult(programAst, List.copyOf(converter.errorsAccum()));
  }

  ZkAst.Program convert(final RustZkParser.FileContext ctx) {
    final List<ZkAst.ProgramItem<ZkAst.ProgramItemData>> allProgramItems =
        ctx.item().stream()
            .map(RustZkParser.ItemContext::programItem)
            .filter(Objects::nonNull)
            .map(this::convert)
            .toList();
    final List<ZkAst.Expression.MacroInvocation> allMacroItems =
        ctx.item().stream()
            .map(RustZkParser.ItemContext::macroInvocationSemi)
            .filter(Objects::nonNull)
            .map(m -> convert(m.macroInvocation()))
            .toList();

    return new ZkAst.Program(
        filterItems(allProgramItems, ZkAst.Function.class),
        filterItems(allProgramItems, ZkAst.ConstantDefinition.class),
        filterItems(allProgramItems, ZkAst.StructDefinition.class),
        filterItems(allProgramItems, ZkAst.UseDeclaration.class),
        filterItems(allProgramItems, ZkAst.TypeAlias.class),
        filterItems(allProgramItems, ZkAst.Module.class),
        allMacroItems);
  }

  ZkAst.ProgramItem<ZkAst.ProgramItemData> convert(
      final RustZkParser.ProgramItemContext uncheckedCtx) {
    final ZkAst.PosInterval pos = pos(uncheckedCtx);
    return new ZkAst.ProgramItem<ZkAst.ProgramItemData>(
        convert(uncheckedCtx.visibility()),
        uncheckedCtx.outerAttribute().stream().map(this::convert).toList(),
        convert(uncheckedCtx.visItem()),
        pos);
  }

  ZkAst.Expression.MacroInvocation convert(final RustZkParser.MacroInvocationContext uncheckedCtx) {
    final ZkAst.PosInterval pos = pos(uncheckedCtx);

    return new ZkAst.Expression.MacroInvocation(
        convertToImmutableList(uncheckedCtx.simplePath()),
        convert(uncheckedCtx.callValueArguments()),
        pos);
  }

  ZkAst.ItemVisibility convert(final RustZkParser.VisibilityContext ctx) {
    if (ctx instanceof RustZkParser.VisibilityPubContext) {
      return ZkAst.ItemVisibility.PUBLIC;
    } else if (ctx instanceof RustZkParser.VisibilityCrateContext) {
      return ZkAst.ItemVisibility.ONLY_CRATE;
    } else {
      return ZkAst.ItemVisibility.ONLY_FILE;
    }
  }

  ZkAst.OuterAttribute convert(final RustZkParser.OuterAttributeContext ctx) {
    return new ZkAst.OuterAttribute(convert(ctx.attr().metaItem()));
  }

  ZkAst.MetaItem convert(final RustZkParser.MetaItemContext uncheckedCtx) {
    // Meta item name
    if (uncheckedCtx instanceof RustZkParser.MetaItemNameContext ctx) {
      return new ZkAst.MetaItem.Name(convertToImmutableList(ctx.simplePath()));

      // Meta item assign
    } else if (uncheckedCtx instanceof RustZkParser.MetaItemAssignContext ctx) {
      return new ZkAst.MetaItem.Assign(
          new ZkAst.MetaItem.Name(convertToImmutableList(ctx.simplePath())),
          convert(ctx.expression()));

      // Meta item call
    } else {
      final RustZkParser.MetaItemCallContext ctx = (RustZkParser.MetaItemCallContext) uncheckedCtx;
      return new ZkAst.MetaItem.Call(
          new ZkAst.MetaItem.Name(convertToImmutableList(ctx.simplePath())),
          ctx.metaItemInner().stream().map(this::convert).toList());
    }
  }

  ZkAst.MetaItemInner convert(final RustZkParser.MetaItemInnerContext uncheckedCtx) {
    if (uncheckedCtx.metaItem() != null) {
      return new ZkAst.MetaItemInner.MetaItem(convert(uncheckedCtx.metaItem()));
    } else {
      return new ZkAst.MetaItemInner.Expression(convert(uncheckedCtx.expression()));
    }
  }

  ZkAst.ProgramItemData convert(final RustZkParser.VisItemContext ctx) {
    // Is function
    if (ctx.function() != null) {
      return convert(ctx.function());

      // Is struct
    } else if (ctx.struct() != null) {
      return convert(ctx.struct());

      // Is Constant
    } else if (ctx.constantItem() != null) {
      return convert(ctx.constantItem());

      // Is type alias
    } else if (ctx.typeAliasItem() != null) {
      return convert(ctx.typeAliasItem());

      // Is Use declaration
    } else if (ctx.useDeclaration() != null) {
      return convert(ctx.useDeclaration());

      // Is Module
    } else {
      return convert(ctx.module());
    }
  }

  ZkAst.ConstantDefinition convert(final RustZkParser.ConstantItemContext ctx) {
    final ZkAst.PosInterval pos = pos(ctx.CONST(), ctx.type());
    return new ZkAst.ConstantDefinition(
        ctx.IDENTIFIER().getText(), convert(ctx.type()), convert(ctx.expression()), pos);
  }

  ZkAst.TypeAlias convert(final RustZkParser.TypeAliasItemContext ctx) {
    final ZkAst.PosInterval pos = pos(ctx.IDENTIFIER());
    return new ZkAst.TypeAlias(ctx.IDENTIFIER().getText(), convert(ctx.type()), pos);
  }

  ZkAst.UseDeclaration convert(final RustZkParser.UseDeclarationContext ctx) {
    final ZkAst.PosInterval pos = pos(ctx.USE(), ctx.SEMICOLON());

    // Determine module path
    final RustZkParser.UseTreeContext tree = ctx.useTree();
    final List<String> modulePath = convertToMutableList(tree.simplePath());

    // Determine imported elements from given module
    final List<String> usedElements;
    if (tree.STAR() != null) {
      usedElements = null;
    } else if (tree.CURLY_LEFT() != null) {
      usedElements = tree.IDENTIFIER().stream().map(elem -> elem.getText()).toList();
    } else {
      usedElements = List.of(modulePath.remove(modulePath.size() - 1));
    }

    // Produce declaration
    return new ZkAst.UseDeclaration(List.copyOf(modulePath), usedElements, pos);
  }

  ZkAst.Module convert(final RustZkParser.ModuleContext ctx) {
    final ZkAst.PosInterval pos = pos(ctx.MOD(), ctx.SEMICOLON());

    return new ZkAst.Module(ctx.IDENTIFIER().getText(), pos);
  }

  ZkAst.StructDefinition convert(final RustZkParser.StructContext ctx) {

    final List<ZkAst.GenericTypeParam> genericParams =
        ctx.genericParams().genericParam().stream().map(this::convert).toList();

    final List<ZkAst.StructField> structFields =
        ctx.structFields().structField().stream().map(this::convert).toList();

    final ZkAst.PosInterval pos = pos(ctx.STRUCT(), ctx.IDENTIFIER());

    return new ZkAst.StructDefinition(ctx.IDENTIFIER().getText(), genericParams, structFields, pos);
  }

  ZkAst.GenericTypeParam convert(final RustZkParser.GenericParamContext ctx) {
    if (!ctx.outerAttribute().isEmpty()) {
      throw new UnsupportedOperationException("Not supported: Outer attributes");
    }

    String traitName = null;
    if (ctx.genericParamType().IDENTIFIER().size() == 2) {
      traitName = ctx.genericParamType().IDENTIFIER().get(1).getText();
    }

    return new ZkAst.GenericTypeParam(
        ctx.genericParamType().IDENTIFIER().get(0).getText(), traitName, pos(ctx));
  }

  ZkAst.StructField convert(final RustZkParser.StructFieldContext ctx) {
    if (!ctx.outerAttribute().isEmpty()) {
      throw new UnsupportedOperationException("Not supported: Outer attributes");
    }
    return new ZkAst.StructField(ctx.IDENTIFIER().getText(), convert(ctx.type()), pos(ctx));
  }

  ZkAst.Function convert(final RustZkParser.FunctionContext ctx) {
    final RustZkParser.BlockExprContext functionBody = ctx.blockExpr();
    if (functionBody == null) {
      throw new UnsupportedOperationException("Not supported: Function declaration without body");
    }

    final ZkAst.PosInterval pos = pos(ctx.FN(), ctx.PAR_RIGHT());

    return new ZkAst.Function(
        ctx.IDENTIFIER().getText(),
        ctx.functionParameters() != null ? convert(ctx.functionParameters()) : List.of(),
        ctx.functionReturnType() != null ? convert(ctx.functionReturnType()) : ZkAst.Type.unit(pos),
        convert(functionBody),
        pos);
  }

  List<ZkAst.Parameter> convert(final RustZkParser.FunctionParametersContext ctx) {
    return ctx.functionParam().stream().map(this::convert).toList();
  }

  ZkAst.Parameter convert(final RustZkParser.FunctionParamContext ctx) {
    if (!ctx.outerAttribute().isEmpty()) {
      throw new UnsupportedOperationException("Not supported: Outer attributes");
    }
    return new ZkAst.Parameter(
        ctx.IDENTIFIER().getText(), convert(ctx.type()), ctx.mut() != null, pos(ctx));
  }

  ZkAst.Type convert(final RustZkParser.FunctionReturnTypeContext ctx) {
    return convert(ctx.type());
  }

  ZkAst.Type convert(final RustZkParser.TypeContext uncheckedCtx) {
    // Array
    if (uncheckedCtx instanceof RustZkParser.TypeArrayContext ctx) {
      if (ctx.expression() == null) {
        return new ZkAst.Type.Slice(convert(ctx.type()), pos(ctx));
      } else {
        return new ZkAst.Type.Array(convert(ctx.type()), convert(ctx.expression()), pos(ctx));
      }

      // Named Type
    } else if (uncheckedCtx instanceof RustZkParser.TypeNamedContext ctx) {
      return new ZkAst.Type.Named(convertToPath(ctx.pathInType()), pos(ctx));

      // Unit type
    } else if (uncheckedCtx instanceof RustZkParser.TypeUnitContext ctx) {
      return ZkAst.Type.unit(pos(ctx));

      // Tuple type
    } else if (uncheckedCtx instanceof RustZkParser.TypeTupleContext ctx) {
      final List<ZkAst.Type> types = ctx.type().stream().map(this::convert).toList();
      return new ZkAst.Type.Tuple(types, pos(ctx));

      // Reference
    } else {
      final RustZkParser.TypeReferenceContext ctx =
          (RustZkParser.TypeReferenceContext) uncheckedCtx;
      return new ZkAst.Type.Reference(convert(ctx.type()), ctx.mut() != null, pos(ctx));
    }
  }

  ZkAst.Expression convert(final RustZkParser.BlockExprContext ctx) {
    if (!ctx.innerAttribute().isEmpty()) {
      throw new UnsupportedOperationException("Not supported: Inner attributes");
    }
    return convert(ctx.statements(), pos(ctx.CURLY_LEFT(), ctx.CURLY_RIGHT()));
  }

  ZkAst.Expression.Block convert(
      final RustZkParser.StatementsContext uncheckedCtx, final ZkAst.PosInterval pos) {
    if (uncheckedCtx instanceof RustZkParser.StatementsWithExprContext ctx) {
      final List<ZkAst.Statement> statements =
          ctx.statement().stream().map(this::convertStatement).toList();
      final ZkAst.Expression returnExpr = convert(ctx.expression());
      return new ZkAst.Expression.Block(statements, returnExpr, pos);
    } else {
      final var ctx = (RustZkParser.StatementsWithoutExprContext) uncheckedCtx;
      final List<ZkAst.Statement> statements =
          ctx.statement().stream().map(this::convertStatement).toList();
      return new ZkAst.Expression.Block(statements, ZkAst.Expression.unitLiteral(pos), pos);
    }
  }

  ZkAst.Expression convert(final RustZkParser.ExpressionContext uncheckedCtx) {

    final ZkAst.PosInterval pos = pos(uncheckedCtx);

    // Literal Int
    if (uncheckedCtx instanceof RustZkParser.ExprLiteralIntContext ctx) {
      // Need to reparse the literal in order to find integers and types.
      return parseIntLiteral(ctx.intLiteral().getText(), pos);

      // Literal Bool
    } else if (uncheckedCtx instanceof RustZkParser.ExprLiteralBoolContext ctx) {
      final boolean value = ctx.getText().equals("true");
      return new ZkAst.Expression.BoolLiteral(value, pos);

      // Literal String
    } else if (uncheckedCtx instanceof RustZkParser.ExprLiteralStringContext ctx) {
      final String value = ctx.STRING().getText();
      return new ZkAst.Expression.StringLiteral(value.substring(1, value.length() - 1), pos);

      // Identifier
    } else if (uncheckedCtx instanceof RustZkParser.ExprIdentifierContext ctx) {
      return new ZkAst.Expression.Variable(convertToPath(ctx.pathInExpression()), pos);

      // Unary expressions
    } else if (uncheckedCtx instanceof RustZkParser.ExprUnaryContext ctx) {
      final String operatorText = ctx.children.get(0).getText();
      return new ZkAst.Expression.Unary(
          UNARY_OP_MAP.get(operatorText), convert(ctx.expression()), pos);

      // Binary expressions
    } else if (uncheckedCtx instanceof RustZkParser.ExprBinaryContext ctx) {
      String operatorText = ctx.children.get(1).getText();

      // Recognize the lexically ambiguous operators "<<", ">>", "||", "&&" which is represented by
      // two separate tokens
      if (ctx.getChildCount() == 4) {
        // Ensure that the two operator tokens are not separated by spaces
        final CommonToken op1 = (CommonToken) ((TerminalNode) ctx.children.get(1)).getSymbol();
        final CommonToken op2 = (CommonToken) ((TerminalNode) ctx.children.get(2)).getSymbol();
        final String operator2Text = ctx.children.get(2).getText();
        if (op1.getLine() != op2.getLine() || op1.getStopIndex() + 1 != op2.getStartIndex()) {
          throw new UnsupportedOperationException(
              "Binary operator not supported: " + operatorText + " " + operator2Text);
        }
        // Concatenate to create the resulting operator
        operatorText += operator2Text;
      }
      return new ZkAst.Expression.Binary(
          BINARY_OP_MAP.get(operatorText),
          convert(ctx.expression(0)),
          convert(ctx.expression(1)),
          pos);

      // Array index expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprArrayIndexContext ctx) {
      return new ZkAst.Expression.Binary(
          ZkAst.Binop.INDEX, convert(ctx.expression(0)), convert(ctx.expression(1)), pos);

      // Call expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprCallContext ctx) {
      return new ZkAst.Expression.Call(
          convert(ctx.expression()), convert(ctx.callValueArguments()), pos);

      // Method call expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprMethodCallContext ctx) {
      final ZkAst.Expression objectExpr = convert(ctx.expression());
      return new ZkAst.Expression.MethodCall(
          objectExpr, ctx.IDENTIFIER().getText(), convert(ctx.callValueArguments()), pos);

      // Macro Invocation expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprMacroInvokationContext ctx) {
      return convert(ctx.macroInvocation());

      // If-expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprIfContext ctx) {
      return convert(ctx.ifExpression());

      // For-expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprForContext ctx) {
      return convert(ctx.forExpression());

      // Assign expressions
    } else if (uncheckedCtx instanceof RustZkParser.ExprAssignContext ctx) {
      return new ZkAst.Expression.Assign(
          convert(ctx.expression().get(0)), convert(ctx.expression().get(1)), pos);

      // Unit expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprUnitContext ctx) {
      return ZkAst.Expression.unitLiteral(pos);

      // Tuple construction expressions
    } else if (uncheckedCtx instanceof RustZkParser.ExprTupleContext ctx) {
      final List<ZkAst.Expression> expressions =
          ctx.expression().stream().map(this::convert).toList();
      return new ZkAst.Expression.TupleConstructor(expressions, pos);

      // Tuple indexing expressions
    } else if (uncheckedCtx instanceof RustZkParser.ExprTupleIndexContext ctx) {
      final ZkAst.Expression.IntLiteral indexExpresssion =
          parseIntLiteral(ctx.INT_LITERAL_DEC().getText(), pos);
      final BigInteger index = indexExpresssion.value();
      return new ZkAst.Expression.TupleAccess(convert(ctx.expression()), index, pos);

      // Field indexing expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprFieldContext ctx) {
      return new ZkAst.Expression.FieldAccess(
          convert(ctx.expression()), ctx.IDENTIFIER().getText(), pos);

      // Type casting expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprTypeCastContext ctx) {
      return new ZkAst.Expression.TypeCast(convert(ctx.expression()), convert(ctx.type()), pos);

      // Block expressions
    } else if (uncheckedCtx instanceof RustZkParser.ExprBlockContext ctx) {
      return convert(ctx.blockExpr());

      // Expression construction expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprStructConstructorContext ctx) {
      return convert(ctx.structExpression());

      // Break expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprBreakContext ctx) {
      return new ZkAst.Expression.Break(pos);

      // Continue expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprContinueContext ctx) {
      return new ZkAst.Expression.Continue(pos);

      // Array constructor
    } else if (uncheckedCtx instanceof RustZkParser.ExprArrayConstructorContext ctx) {

      if (ctx.arrayElements()
          instanceof RustZkParser.ArrayElementsRepeatedContext elementsContext) {
        return new ZkAst.Expression.ArrayConstructorRepeat(
            convert(elementsContext.expression().get(0)),
            convert(elementsContext.expression().get(1)),
            pos);

      } else {
        final var elementsContext = (RustZkParser.ArrayElementsExplicitContext) ctx.arrayElements();
        return new ZkAst.Expression.ArrayConstructorExplicit(
            elementsContext.expression().stream().map(this::convert).toList(), pos);
      }

      // Parens expressions
    } else {
      final RustZkParser.ExprParContext ctx = (RustZkParser.ExprParContext) uncheckedCtx;
      return convert(ctx.expression());
    }
  }

  ZkAst.Expression.StructConstructor convert(final RustZkParser.StructExpressionContext ctxStruct) {
    final List<ZkAst.Expression.StructConstructor.Field> fields =
        ctxStruct.structExpressionFields().structExpressionField().stream()
            .map(
                fieldCtx -> {
                  final ZkAst.Expression fieldExpr;
                  if (fieldCtx.expression() != null) {
                    fieldExpr = convert(fieldCtx.expression());
                  } else {
                    fieldExpr =
                        new ZkAst.Expression.Variable(
                            new ZkAst.Path(
                                List.of(fieldCtx.IDENTIFIER().getText()), List.of(), pos(fieldCtx)),
                            pos(fieldCtx));
                  }
                  return new ZkAst.Expression.StructConstructor.Field(
                      fieldCtx.IDENTIFIER().getText(), fieldExpr, pos(fieldCtx));
                })
            .toList();

    return new ZkAst.Expression.StructConstructor(
        convertToPath(ctxStruct.pathInExpression()), fields, pos(ctxStruct));
  }

  List<ZkAst.Expression> convert(final RustZkParser.CallValueArgumentsContext ctx) {
    return ctx.expression().stream().map(this::convert).toList();
  }

  ZkAst.Expression.If convert(final RustZkParser.IfExpressionContext ctx) {
    final var pos = pos(ctx.IF(), ctx.expression());
    final ZkAst.Expression condExpr = convert(ctx.expression());
    final ZkAst.Expression thenExpr = convert(ctx.blockExpr().get(0));
    final ZkAst.Expression elseExpr;
    if (ctx.ifExpression() != null) {
      elseExpr = convert(ctx.ifExpression());
    } else if (ctx.blockExpr().size() > 1) {
      elseExpr = convert(ctx.blockExpr().get(1));
    } else {
      elseExpr = ZkAst.Expression.unitLiteral(pos);
    }
    return new ZkAst.Expression.If(condExpr, thenExpr, elseExpr, pos);
  }

  ZkAst.Expression.For convert(final RustZkParser.ForExpressionContext ctx) {
    final ZkAst.Expression iteratorInitExpr = convert(ctx.expression());
    final ZkAst.Expression blockBody = convert(ctx.blockExpr());
    return new ZkAst.Expression.For(
        ctx.IDENTIFIER().getText(), iteratorInitExpr, blockBody, pos(ctx));
  }

  List<ZkAst.Type> convert(final RustZkParser.PathGenericsContext ctx) {
    return ctx == null ? List.of() : ctx.type().stream().map(this::convert).toList();
  }

  static final Map<String, ZkAst.Binop> BINARY_OP_MAP =
      Map.ofEntries(
          // Arithmetic
          entry("*", ZkAst.Binop.MULT),
          entry("/", ZkAst.Binop.DIV),
          entry("%", ZkAst.Binop.REMAINDER),
          entry("+", ZkAst.Binop.PLUS),
          entry("-", ZkAst.Binop.MINUS),

          // Bitwise
          entry("<<", ZkAst.Binop.BITSHIFT_LEFT),
          entry(">>", ZkAst.Binop.BITSHIFT_RIGHT),
          entry("&", ZkAst.Binop.BITWISE_AND),
          entry("^", ZkAst.Binop.BITWISE_XOR),
          entry("|", ZkAst.Binop.BITWISE_OR),
          // Logical
          entry("&&", ZkAst.Binop.LOGICAL_AND),
          entry("||", ZkAst.Binop.LOGICAL_OR),

          // Equality
          entry("==", ZkAst.Binop.EQUAL),
          entry("!=", ZkAst.Binop.NOT_EQUAL),

          // Comparison
          entry("<", ZkAst.Binop.LESS_THAN),
          entry(">", ZkAst.Binop.GREATER_THAN),
          entry("<=", ZkAst.Binop.LESS_THAN_OR_EQUAL),
          entry(">=", ZkAst.Binop.GREATER_THAN_OR_EQUAL),

          // Misc
          entry("..", ZkAst.Binop.RANGE));

  static final Map<String, ZkAst.Unop> UNARY_OP_MAP =
      Map.ofEntries(entry("-", ZkAst.Unop.NEGATE), entry("!", ZkAst.Unop.BITWISE_NOT));

  ZkAst.Statement convertStatement(final RustZkParser.StatementContext uncheckedCtx) {
    final ZkAst.PosInterval pos = pos(uncheckedCtx);

    // Return
    if (uncheckedCtx instanceof RustZkParser.ReturnStatementContext ctx) {
      return new ZkAst.Statement.Return(convert(ctx.expression()), pos);

      // Expression
    } else if (uncheckedCtx instanceof RustZkParser.ExprStatementContext ctx) {
      final ZkAst.Expression innerExpression = convert(ctx.expression());
      return new ZkAst.Statement.Expr(innerExpression, pos);

      // Let statement
    } else if (uncheckedCtx instanceof RustZkParser.LetStatementContext ctx) {
      if (!ctx.outerAttribute().isEmpty()) {
        throw new UnsupportedOperationException("Not supported: Inner attributes");
      }

      final RustZkParser.TypeContext declaredType = ctx.type();

      return new ZkAst.Statement.Let(
          ctx.IDENTIFIER().getText(),
          ctx.mut() != null,
          declaredType == null ? null : convert(declaredType),
          convert(ctx.expression()),
          pos);

      // If-statement
    } else if (uncheckedCtx instanceof RustZkParser.IfStatementContext ctx) {
      final var innerExpression = convert(ctx.ifExpression());
      return new ZkAst.Statement.Expr(innerExpression, pos);

      // Empty statement
    } else if (uncheckedCtx instanceof RustZkParser.EmptyStatementContext ctx) {
      return new ZkAst.Statement.Expr(ZkAst.Expression.unitLiteral(pos), pos);

      // For-statement
    } else {
      final RustZkParser.ForStatementContext ctx = (RustZkParser.ForStatementContext) uncheckedCtx;
      final var innerExpression = convert(ctx.forExpression());
      return new ZkAst.Statement.Expr(innerExpression, pos);
    }
  }

  static List<String> convertToImmutableList(final RustZkParser.SimplePathContext ctx) {
    return ctx.simplePathSegment().stream().map(segment -> segment.IDENTIFIER().getText()).toList();
  }

  static List<String> convertToMutableList(final RustZkParser.SimplePathContext ctx) {
    return ctx.simplePathSegment().stream()
        .map(segment -> segment.getText())
        .collect(Collectors.toList());
  }

  ZkAst.Path convertToPath(final RustZkParser.PathInExpressionContext ctx) {
    return new ZkAst.Path(
        convertToImmutableList(ctx.simplePath()), convert(ctx.pathGenerics()), pos(ctx));
  }

  ZkAst.Path convertToPath(final RustZkParser.PathInTypeContext ctx) {
    return new ZkAst.Path(
        convertToImmutableList(ctx.simplePath()), convert(ctx.pathGenerics()), pos(ctx));
  }

  private static final Pattern RUST_INTEGER_LITERAL_PATTERN =
      Pattern.compile("^([0-9a-fA-F_]+)(\\w+)?$");

  /**
   * Parses given integer literal to an AST element.
   *
   * @param integerStr Integer string to be parsed.
   * @param pos Position to use for AST element.
   * @return Newly created AST element.
   */
  ZkAst.Expression.IntLiteral parseIntLiteral(
      final String integerStr, final ZkAst.PosInterval pos) {

    // Determine radix
    final int radix;
    final String integerStrWithoutPrefix;
    if (integerStr.startsWith("0b")) {
      radix = 2;
      integerStrWithoutPrefix = integerStr.substring(2);
    } else if (integerStr.startsWith("0o")) {
      radix = 8;
      integerStrWithoutPrefix = integerStr.substring(2);
    } else if (integerStr.startsWith("0x")) {
      radix = 16;
      integerStrWithoutPrefix = integerStr.substring(2);
    } else {
      radix = 10;
      integerStrWithoutPrefix = integerStr;
    }

    // Determine suffix
    final Matcher matcher = RUST_INTEGER_LITERAL_PATTERN.matcher(integerStrWithoutPrefix);
    matcher.matches();
    final BigInteger value;
    try {
      value = new BigInteger(matcher.group(1).replace("_", ""), radix);
    } catch (NumberFormatException e) {
      errorsAccum.add(
          new RustZk.ErrorLine("Bad literal integer format: %s".formatted(integerStr), pos));
      return new ZkAst.Expression.IntLiteral(BigInteger.ZERO, null, pos);
    }
    final String explicitTypeStr = matcher.group(2);

    final ZkAst.Type.Named explicitType =
        explicitTypeStr != null
            ? new ZkAst.Type.Named(ZkAst.Path.simple(pos, explicitTypeStr), pos)
            : null;
    return new ZkAst.Expression.IntLiteral(value, explicitType, pos);
  }

  //// Token utility

  private static TerminalNode firstToken(final Tree tree) {
    if (tree instanceof TerminalNode node) {
      return node;
    }
    return firstToken(tree.getChild(0));
  }

  private static TerminalNode lastToken(final Tree tree) {
    if (tree instanceof TerminalNode node) {
      return node;
    }
    return lastToken(tree.getChild(tree.getChildCount() - 1));
  }

  private static ZkAst.PosInterval pos(final Tree tree) {
    return posToken(firstToken(tree), lastToken(tree));
  }

  private static ZkAst.PosInterval pos(final Tree nodeStart, final Tree nodeEnd) {
    return posToken(firstToken(nodeStart), lastToken(nodeEnd));
  }

  private static ZkAst.PosInterval posToken(
      final TerminalNode nodeStart, final TerminalNode nodeEnd) {
    final var symStart = nodeStart.getSymbol();
    final var symEnd = nodeEnd.getSymbol();
    return new ZkAst.PosInterval(
        symStart.getLine(),
        symStart.getCharPositionInLine(),
        symEnd.getLine(),
        symEnd.getCharPositionInLine() + symEnd.getText().length());
  }
}
