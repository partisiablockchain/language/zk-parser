package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.immutabledetect.ImmutableDetect;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/** Test the RustZk parser. */
public final class RustZkParseTest {

  @Test
  public void testErrorListener() {
    final RustZk.SyntaxErrorListener errorListener = new RustZk.SyntaxErrorListener();
    final var parser = RustZk.createParser("", "", errorListener);
    Assertions.assertThat(parser.getErrorListeners())
        .hasSize(1)
        .first()
        .isInstanceOf(RustZk.SyntaxErrorListener.class)
        .isEqualTo(errorListener);
  }

  static Stream<Arguments> provideTestCasesSuccess() {
    return ExamplePrograms.TEST_CASES_SUCCESS.stream().map(Arguments::of);
  }

  /**
   * Test that the parsing succeeds for a file.
   *
   * @param filename The file name
   */
  @ParameterizedTest
  @MethodSource("provideTestCasesSuccess")
  public void parseValid(String filename) {
    final String source = ExamplePrograms.loadProgramText(filename);
    final ZkAst.Program program = RustZk.parseOrThrow(source, filename);
    Assertions.assertThat(program)
        .as(filename)
        .matches(ImmutableDetect::isImmutable, "is immutable");
  }

  static Stream<Arguments> provideTestCasesUnsupported() {
    return ExamplePrograms.TEST_CASES_UNSUPPORTED.stream().map(Arguments::of);
  }

  /**
   * Test that parsing is not supported for a file.
   *
   * @param filename The file name
   */
  @ParameterizedTest
  @MethodSource("provideTestCasesUnsupported")
  public void parseUnsupported(String filename) {
    final String source = ExamplePrograms.loadProgramText(filename);
    Assertions.assertThatCode(() -> RustZk.parse(source, filename))
        .as(filename)
        .isInstanceOf(UnsupportedOperationException.class);
  }

  static Stream<Arguments> provideTestCasesSyntaxError() {
    return ExamplePrograms.TEST_CASES_SYNTAXERROR.stream().map(Arguments::of);
  }

  /**
   * Test that the parsing results in syntax error for a file.
   *
   * @param filename The file name
   */
  @ParameterizedTest
  @MethodSource("provideTestCasesSyntaxError")
  public void parseSyntaxError(String filename) {
    final String source = ExamplePrograms.loadProgramText(filename);
    final String expectedError = parseExpectedErrorFormat(source);
    Assertions.assertThat(expectedError).as("error format in " + filename).isNotNull().isNotEmpty();

    final RustZk.ParseResult parseResult = RustZk.parse(source, filename);

    Assertions.assertThat(parseResult).as(filename).isNotNull();
    Assertions.assertThat(parseResult.syntaxErrors()).as(filename).isNotEmpty();
    Assertions.assertThat(parseResult.syntaxErrors().get(0).message())
        .as(filename)
        .startsWith(expectedError);
    Assertions.assertThat(parseResult.program()).as(filename).isNull();
  }

  private static String parseExpectedErrorFormat(String source) {
    final var pattern = Pattern.compile("//!\\s*assert\\s+fail_parse\\s*\"(.*)\"");
    final Matcher matcher = pattern.matcher(source);
    if (matcher.find()) {
      return matcher.group(1);
    } else {
      return null;
    }
  }

  static Stream<Arguments> provideTestCasesWip() {
    return ExamplePrograms.TEST_CASES_WIP.stream().map(Arguments::of);
  }

  /**
   * Test that the parsing fails for a file. The WIP files contain functionality that we want to add
   * to the parser.
   *
   * @param filename The file name
   */
  @ParameterizedTest
  @MethodSource("provideTestCasesWip")
  public void parseWorkInProgress(String filename) {
    final String source = ExamplePrograms.loadProgramText(filename);
    Assertions.assertThatCode(() -> RustZk.parseOrThrow(source, filename))
        .as(filename)
        .isInstanceOf(RuntimeException.class);
  }
}
