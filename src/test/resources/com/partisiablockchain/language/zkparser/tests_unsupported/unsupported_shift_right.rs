// Fails because >> is separated by a space
pub fn add_self(x: i32) -> i32 {
    x > > 4
}
