package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.math.BigInteger;
import java.util.List;

/** Namespace class for Zk AST elements. */
public final class ZkAst {

  private ZkAst() {}

  /** Represents entire Zk program. */
  public record Program(
      List<ProgramItem<Function>> functions,
      List<ProgramItem<ConstantDefinition>> constants,
      List<ProgramItem<StructDefinition>> structs,
      List<ProgramItem<UseDeclaration>> useDeclarations,
      List<ProgramItem<TypeAlias>> typeAliases,
      List<ProgramItem<Module>> modules,
      List<Expression.MacroInvocation> macroItems) {

    /**
     * Create new program AST element.
     *
     * @param functions Function definitions in a program.
     * @param constants Constant definitions in a program.
     * @param structs Struct definitions in a program.
     * @param useDeclarations Use declarations in program.
     * @param typeAliases Type alias declarations in program.
     * @param modules Modules in program.
     * @param macroItems Top level macro invocations in program.
     */
    public Program {
      requireNonNull(functions);
      requireNonNull(constants);
      requireNonNull(structs);
      requireNonNull(useDeclarations);
      requireNonNull(typeAliases);
      requireNonNull(modules);
      requireNonNull(macroItems);
    }
  }

  /** Represents visibility of {@link ProgramItemData}. */
  public enum ItemVisibility {
    /** Item is only visible within file it was defined in. */
    ONLY_FILE,
    /** Item is only visible within crate it was defined in. */
    ONLY_CRATE,
    /** Item is visible to anything, anywhere. */
    PUBLIC,
  }

  /**
   * Represents outer attributes.
   *
   * @see <a href="https://doc.rust-lang.org/reference/attributes.html">The Rust Reference,
   *     Attributes</a>
   */
  public record OuterAttribute(MetaItem metaItem) {}

  /**
   * Represents meta-item syntax items.
   *
   * @see <a
   *     href="https://doc.rust-lang.org/reference/attributes.html#meta-item-attribute-syntax">The
   *     Rust Reference, Meta Item Attribute Syntax</a>
   */
  public interface MetaItem {
    /**
     * Represents meta-item syntax items.
     *
     * @see <a
     *     href="https://doc.rust-lang.org/reference/attributes.html#meta-item-attribute-syntax">The
     *     Rust Reference, Meta Item Attribute Syntax</a>
     */
    record Name(List<String> path) implements MetaItem {}

    /**
     * Represents meta-item syntax items.
     *
     * @see <a
     *     href="https://doc.rust-lang.org/reference/attributes.html#meta-item-attribute-syntax">The
     *     Rust Reference, Meta Item Attribute Syntax</a>
     */
    record Assign(Name name, Expression value) implements MetaItem {}

    /**
     * Represents meta-item syntax items.
     *
     * @see <a
     *     href="https://doc.rust-lang.org/reference/attributes.html#meta-item-attribute-syntax">The
     *     Rust Reference, Meta Item Attribute Syntax</a>
     */
    record Call(Name name, List<MetaItemInner> arguments) implements MetaItem {}
  }

  /**
   * Represents meta-item syntax items.
   *
   * @see <a
   *     href="https://doc.rust-lang.org/reference/attributes.html#meta-item-attribute-syntax">The
   *     Rust Reference, Meta Item Attribute Syntax</a>
   */
  public interface MetaItemInner {
    /**
     * Represents meta-item syntax items.
     *
     * @see <a
     *     href="https://doc.rust-lang.org/reference/attributes.html#meta-item-attribute-syntax">The
     *     Rust Reference, Meta Item Attribute Syntax</a>
     */
    record MetaItem(ZkAst.MetaItem metaItem) implements MetaItemInner {}

    /**
     * Represents meta-item syntax items.
     *
     * @see <a
     *     href="https://doc.rust-lang.org/reference/attributes.html#meta-item-attribute-syntax">The
     *     Rust Reference, Meta Item Attribute Syntax</a>
     */
    record Expression(ZkAst.Expression expression) implements MetaItemInner {}
  }

  /** Represents top-level item in Zk program, including visibility and attributes. */
  public record ProgramItem<I extends ProgramItemData>(
      ItemVisibility visibility, List<OuterAttribute> attributes, I item, PosInterval position)
      implements HasPosition {

    /**
     * Constructor for {@link ProgramItem}.
     *
     * @param visibility The visibility level of this item.
     * @param attributes Attributes on time.
     * @param item The item itself.
     * @param position Source code position of item.
     */
    public ProgramItem {
      requireNonNull(visibility);
      requireNonNull(attributes);
      requireNonNull(item);
      requireNonNull(position);
    }
  }

  /** Represents top-level item in Zk program, without visibility and attributes. */
  public sealed interface ProgramItemData extends HasPosition {}

  /** Represents function in Zk program. */
  public record Function(
      String identifier,
      List<Parameter> parameters,
      Type returnType,
      Expression body,
      PosInterval position)
      implements ProgramItemData {

    /**
     * Create new function AST element.
     *
     * @param identifier identifier of function
     * @param parameters parameters of function
     * @param returnType returnType of function
     * @param body body of function
     * @param position Position of AST element in source code.
     */
    public Function {
      requireNonNull(identifier);
      requireNonNull(parameters);
      requireNonNull(returnType);
      requireNonNull(body);
      requireNonNull(position);
    }
  }

  /** Represents constant item in a Zk Program. */
  public record ConstantDefinition(
      String identifier, Type type, Expression initExpr, PosInterval position)
      implements ProgramItemData {

    /**
     * Create new constant definition AST element.
     *
     * @param identifier identifier of constant definition
     * @param type type of constant definition
     * @param initExpr initExpr of constant definition
     * @param position Position of AST element in source code.
     */
    public ConstantDefinition {
      requireNonNull(identifier);
      requireNonNull(type);
      requireNonNull(initExpr);
      requireNonNull(position);
    }
  }

  /** Represents struct item in a Zk Program. */
  public record StructDefinition(
      String structIdentifier,
      List<GenericTypeParam> genericTypeParams,
      List<StructField> fields,
      PosInterval position)
      implements ProgramItemData {

    /**
     * Create new struct definition AST element.
     *
     * @param structIdentifier Type name of the struct.
     * @param genericTypeParams Binding declarations for generic types.
     * @param fields Fields of the struct.
     * @param position Position of AST element in source code.
     */
    public StructDefinition {
      requireNonNull(structIdentifier);
      requireNonNull(genericTypeParams);
      requireNonNull(fields);
      requireNonNull(position);
    }
  }

  /** Individual generic type. */
  public record GenericTypeParam(String boundName, String traitConstraint, PosInterval position)
      implements HasPosition {

    /**
     * Create new generic type parameter AST element.
     *
     * @param boundName Name of the bound type. Never null.
     * @param traitConstraint Declares type as being of the given trait. May be null to convey that
     *     no trait constraint was given.
     * @param position Position of AST element in source code.
     */
    public GenericTypeParam {
      requireNonNull(boundName);
      requireNonNull(position);
    }
  }

  /** Individual fields of a struct. */
  public record StructField(String fieldName, Type fieldType, PosInterval position) {

    /**
     * Create new struct field AST element.
     *
     * @param fieldName Name of the field.
     * @param fieldType Type of the field.
     * @param position Position of AST element in source code.
     */
    public StructField {
      requireNonNull(fieldName);
      requireNonNull(fieldType);
      requireNonNull(position);
    }
  }

  /** Represents parameter in Zk function. */
  public record Parameter(String identifier, Type type, boolean isMutable, PosInterval position)
      implements HasPosition {

    /**
     * Create new parameter AST element.
     *
     * @param identifier identifier of the parameter.
     * @param type type of the parameter.
     * @param isMutable Whether the referenced value is mutable.
     * @param position Position of AST element in source code.
     */
    public Parameter {
      requireNonNull(identifier);
      requireNonNull(type);
      requireNonNull(position);
    }
  }

  /** Usage declaration in Rust program. */
  public record UseDeclaration(
      List<String> modulePath, List<String> moduleElements, PosInterval position)
      implements HasPosition, ProgramItemData {

    /**
     * Constructor for use declaration.
     *
     * @param modulePath Path of the module.
     * @param moduleElements Elements of the module to import. Might be null, which indicate to
     *     import everything.
     * @param position Position of AST element in source code.
     */
    public UseDeclaration {
      requireNonNull(modulePath);
      requireNonNull(position);
    }
  }

  /** Type alias item in Rust program. */
  public record TypeAlias(String aliasIdentifier, Type type, PosInterval position)
      implements HasPosition, ProgramItemData {

    /**
     * Create new type alias AST element.
     *
     * @param aliasIdentifier identifier of type
     * @param type Type to create alias for
     * @param position Position of AST element in source code.
     */
    public TypeAlias {
      requireNonNull(aliasIdentifier);
      requireNonNull(type);
      requireNonNull(position);
    }
  }

  /** Module in Rust program. */
  public record Module(String moduleName, PosInterval position)
      implements HasPosition, ProgramItemData {

    /**
     * Constructor for module.
     *
     * @param moduleName Name of the module.
     * @param position Position of AST element in source code.
     */
    public Module {
      requireNonNull(moduleName);
      requireNonNull(position);
    }
  }

  /** Path in program. */
  public record Path(List<String> path, List<ZkAst.Type> genericTypes, PosInterval position)
      implements HasPosition {

    /**
     * Constructor for path.
     *
     * @param path Simple path.
     * @param genericTypes Generic types attached to last element of path.
     * @param position Position of AST element in source code.
     */
    public Path {
      requireNonNull(path);
      requireNonNull(genericTypes);
      requireNonNull(position);
    }

    /**
     * Constructor for simple path.
     *
     * @param position Position of AST element in source code.
     * @param path Simple path elements.
     * @return Newly created path element.
     */
    static Path simple(PosInterval position, String... path) {
      return new Path(List.of(path), List.of(), position);
    }
  }

  /** Represents secret-less Type in Zk. */
  public sealed interface Type extends HasPosition {

    /**
     * Constructor for unit type.
     *
     * @param position Position of AST element in source code.
     * @return Newly created unit type.
     */
    static Tuple unit(final PosInterval position) {
      return new Tuple(List.of(), position);
    }

    /** Tuple type containing multiple values. */
    public record Tuple(List<Type> subtypes, PosInterval position) implements Type {
      /**
       * Create new tuple AST element.
       *
       * @param subtypes subtypes of the tuple.
       * @param position Position of AST element in source code.
       */
      public Tuple {
        requireNonNull(subtypes);
        requireNonNull(position);
      }
    }

    /** Array types. */
    public record Array(Type baseType, Expression sizeExpr, PosInterval position) implements Type {
      /**
       * Create new array type AST element.
       *
       * @param baseType baseType of the array.
       * @param sizeExpr sizeExpr of the array.
       * @param position Position of AST element in source code.
       */
      public Array {
        requireNonNull(baseType);
        requireNonNull(sizeExpr);
        requireNonNull(position);
      }
    }

    /** Slice types. */
    public record Slice(Type baseType, PosInterval position) implements Type {
      /**
       * Create new slice type AST element.
       *
       * @param baseType baseType of the slice.
       * @param position Position of AST element in source code.
       */
      public Slice {
        requireNonNull(baseType);
        requireNonNull(position);
      }
    }

    /** Reference types. */
    public record Reference(Type baseType, boolean isMutable, PosInterval position)
        implements Type {
      /**
       * Create new reference type AST element.
       *
       * @param baseType Type of referenced value.
       * @param isMutable Whether the referenced value is mutable.
       * @param position Position of AST element in source code.
       */
      public Reference {
        requireNonNull(baseType);
        requireNonNull(position);
      }
    }

    /** A named type. The type is generic if the type parameters is non-empty. */
    public record Named(Path namePath, PosInterval position) implements Type {
      /**
       * Create new named type AST element.
       *
       * @param namePath The type name path.
       * @param position Position of AST element in source code.
       */
      public Named {
        requireNonNull(namePath);
        requireNonNull(position);
      }
    }
  }

  /** Expressions in Zk programs. */
  public sealed interface Expression extends HasPosition {
    /** Block expression, containing statements. */
    public record Block(List<Statement> statements, Expression returnExpr, PosInterval position)
        implements Expression {
      /**
       * Create new block expression AST element.
       *
       * @param statements Statements of the block.
       * @param returnExpr Return expression of the block.
       * @param position Position of AST element in source code.
       */
      public Block {
        requireNonNull(statements);
        requireNonNull(returnExpr);
        requireNonNull(position);
      }
    }

    /** Unary operation on expression. */
    public record Unary(Unop operation, Expression expr1, PosInterval position)
        implements Expression {
      /**
       * Create new unary expression AST element.
       *
       * @param operation Operation itself.
       * @param expr1 Sub Expression
       * @param position Position of AST element in source code.
       */
      public Unary {
        requireNonNull(operation);
        requireNonNull(expr1);
        requireNonNull(position);
      }
    }

    /** Binary operation on expressions. */
    public record Binary(Binop operation, Expression expr1, Expression expr2, PosInterval position)
        implements Expression {
      /**
       * Create new binary expression AST element.
       *
       * @param operation Expression operation
       * @param expr1 Left expression
       * @param expr2 Right expression
       * @param position Position of AST element in source code.
       */
      public Binary {
        requireNonNull(operation);
        requireNonNull(expr1);
        requireNonNull(expr2);
        requireNonNull(position);
      }
    }

    /** Variable expression, fetching value from scope. */
    public record Variable(Path variablePath, PosInterval position) implements Expression {
      /**
       * Create new variable expression AST element.
       *
       * @param variablePath Identifier path of the variable.
       * @param position Position of AST element in source code.
       */
      public Variable {
        requireNonNull(variablePath);
        requireNonNull(position);
      }
    }

    /** Bool literal. */
    public record BoolLiteral(boolean value, PosInterval position) implements Expression {
      /**
       * Create new boolean literal expression AST element.
       *
       * @param value Value of literal
       * @param position Position of AST element in source code.
       */
      public BoolLiteral {
        requireNonNull(position);
      }
    }

    /** Int literal. */
    public record IntLiteral(BigInteger value, Type.Named explicitType, PosInterval position)
        implements Expression {

      /**
       * Create new integer literal expression AST element.
       *
       * @param value Value of the literal.
       * @param explicitType If non-null, represents the desired type of the literal.
       * @param position Position of AST element in source code.
       */
      public IntLiteral {
        requireNonNull(value);
        requireNonNull(position);
      }
    }

    /** String literal. */
    public record StringLiteral(String value, PosInterval position) implements Expression {
      /**
       * Create new string literal expression AST element.
       *
       * @param value String value.
       * @param position Position of AST element in source code.
       */
      public StringLiteral {
        requireNonNull(value);
        requireNonNull(position);
      }
    }

    /**
     * Constructor for unit literal.
     *
     * @param position Position of AST element in source code.
     * @return Newly created unit literal.
     */
    static TupleConstructor unitLiteral(PosInterval position) {
      return new TupleConstructor(List.of(), position);
    }

    /** Tuple constructor. */
    public record TupleConstructor(List<Expression> exprs, PosInterval position)
        implements Expression {
      /**
       * Create new tuple constructor expression AST element.
       *
       * @param exprs Expressions of tuple.
       * @param position Position of AST element in source code.
       */
      public TupleConstructor {
        requireNonNull(exprs);
        requireNonNull(position);
      }
    }

    /** Tuple indexing . */
    public record TupleAccess(Expression tupleExpr, BigInteger index, PosInterval position)
        implements Expression {
      /**
       * Create new tuple access expression AST element.
       *
       * @param tupleExpr Expression to produce value to index into.
       * @param index Index of accessed field in value.
       * @param position Position of AST element in source code.
       */
      public TupleAccess {
        requireNonNull(tupleExpr);
        requireNonNull(index);
        requireNonNull(position);
      }
    }

    /** Function call. */
    public record Call(
        Expression functionExpr, List<Expression> valueArgumentExprs, PosInterval position)
        implements Expression {
      /**
       * Create new call expression AST element.
       *
       * @param functionExpr Expression to produce function value.
       * @param valueArgumentExprs Expressions producing value arguments to call.
       * @param position Position of AST element in source code.
       */
      public Call {
        requireNonNull(functionExpr);
        requireNonNull(valueArgumentExprs);
        requireNonNull(position);
      }
    }

    /** Object method call. */
    public record MethodCall(
        Expression objectExpr,
        String methodIdentifier,
        List<Expression> parameterExprs,
        PosInterval position)
        implements Expression {
      /**
       * Create new method call expression AST element.
       *
       * @param objectExpr Object expression.
       * @param methodIdentifier Method identifier.
       * @param parameterExprs Expressions to parameters.
       * @param position Position of AST element in source code.
       */
      public MethodCall {
        requireNonNull(objectExpr);
        requireNonNull(methodIdentifier);
        requireNonNull(parameterExprs);
        requireNonNull(position);
      }
    }

    /** Macro Invocation. */
    public record MacroInvocation(
        List<String> macroIdentifier, List<Expression> parameterExprs, PosInterval position)
        implements Expression {
      /**
       * Create new macro invocation expression AST element.
       *
       * @param macroIdentifier Macro identifier
       * @param parameterExprs Expressions to parameters.
       * @param position Position of AST element in source code.
       */
      public MacroInvocation {
        requireNonNull(macroIdentifier);
        requireNonNull(parameterExprs);
        requireNonNull(position);
      }
    }

    /** If-expression. */
    public record If(
        Expression condExpr, Expression thenExpr, Expression elseExpr, PosInterval position)
        implements Expression {
      /**
       * Create new if expression AST element.
       *
       * @param condExpr Condition expression.
       * @param thenExpr Else expression.
       * @param elseExpr Else expression.
       * @param position Position of AST element in source code.
       */
      public If {
        requireNonNull(condExpr);
        requireNonNull(thenExpr);
        requireNonNull(elseExpr);
        requireNonNull(position);
      }
    }

    /** For-expression. */
    public record For(
        String iteratorVariableIdentifier,
        Expression iteratorExpr,
        Expression body,
        PosInterval position)
        implements Expression {
      /**
       * Create new for-expression AST element.
       *
       * @param iteratorVariableIdentifier Iterator variable
       * @param iteratorExpr Expression to produce iterable.
       * @param body Body of for loop.
       * @param position Position of AST element in source code.
       */
      public For {
        requireNonNull(iteratorVariableIdentifier);
        requireNonNull(iteratorExpr);
        requireNonNull(body);
        requireNonNull(position);
      }
    }

    /** Field indexing operator. */
    public record FieldAccess(Expression objectExpr, String fieldIdentifier, PosInterval position)
        implements Expression {
      /**
       * Create new field access expression AST element.
       *
       * @param objectExpr Struct expression.
       * @param fieldIdentifier Field identifier.
       * @param position Position of AST element in source code.
       */
      public FieldAccess {
        requireNonNull(objectExpr);
        requireNonNull(fieldIdentifier);
        requireNonNull(position);
      }
    }

    /** Assignment operator. */
    public record Assign(Expression placeExpr, Expression valueExpr, PosInterval position)
        implements Expression {
      /**
       * Create new assign expression AST element.
       *
       * @param placeExpr Expression describing memory to assign.
       * @param valueExpr Expression of assigned value.
       * @param position Position of AST element in source code.
       */
      public Assign {
        requireNonNull(placeExpr);
        requireNonNull(valueExpr);
        requireNonNull(position);
      }
    }

    /** Type cast expression. */
    public record TypeCast(Expression valueExpr, Type resultType, PosInterval position)
        implements Expression {
      /**
       * Create new type cast expression AST element.
       *
       * @param valueExpr Value expression.
       * @param resultType Type to cast to.
       * @param position Position of AST element in source code.
       */
      public TypeCast {
        requireNonNull(valueExpr);
        requireNonNull(resultType);
        requireNonNull(position);
      }
    }

    /** Break expression. */
    public record Break(PosInterval position) implements Expression {
      /**
       * Create new break AST element.
       *
       * @param position Position of AST element in source code.
       */
      public Break {
        requireNonNull(position);
      }
    }

    /** Continue expression. */
    public record Continue(PosInterval position) implements Expression {
      /**
       * Create new continue AST element.
       *
       * @param position Position of AST element in source code.
       */
      public Continue {
        requireNonNull(position);
      }
    }

    /** Expression creates a struct value. */
    public record StructConstructor(Path typePath, List<Field> fieldInits, PosInterval position)
        implements Expression {
      /**
       * Create new struct constructor AST element.
       *
       * @param typePath Path of struct type to create.
       * @param fieldInits Initializers for each field.
       * @param position Position of AST element in source code.
       */
      public StructConstructor {
        requireNonNull(typePath);
        requireNonNull(fieldInits);
        requireNonNull(position);
      }

      /** Individual field initializer within a {@link StructConstructor}. */
      public record Field(String fieldName, Expression fieldInitExpr, PosInterval position)
          implements HasPosition {
        /**
         * Create new field expression AST element.
         *
         * @param fieldName Name of field to initialize.
         * @param fieldInitExpr Expression to evaluate to determine initial value of field.
         * @param position Position of AST element in source code.
         */
        public Field {
          requireNonNull(fieldName);
          requireNonNull(fieldInitExpr);
          requireNonNull(position);
        }
      }
    }

    /** Expression creates a struct value. */
    public record ArrayConstructorRepeat(
        Expression repeatExpression, Expression sizeExpression, PosInterval position)
        implements Expression {

      /**
       * Repeating element array constructor.
       *
       * @param repeatExpression Expression producing repeated value.
       * @param sizeExpression Expression producing size value.
       * @param position Position of AST element in source code.
       */
      public ArrayConstructorRepeat {
        requireNonNull(repeatExpression);
        requireNonNull(sizeExpression);
        requireNonNull(position);
      }
    }

    /** Expression creates a struct value. */
    public record ArrayConstructorExplicit(
        List<Expression> elementExpressions, PosInterval position) implements Expression {

      /**
       * Explicit element array constructor.
       *
       * @param elementExpressions Individual element expressions.
       * @param position Position of AST element in source code.
       */
      public ArrayConstructorExplicit {
        requireNonNull(elementExpressions);
        requireNonNull(position);
      }
    }
  }

  /** Statements in Zk programs. */
  public sealed interface Statement extends HasPosition {
    /** Let statement, binding variable to it's initial value. */
    public record Let(
        String identifier, boolean isMutable, Type type, Expression initExpr, PosInterval position)
        implements Statement {
      /**
       * Create new let statement AST element.
       *
       * @param identifier Identifier to define. Never null.
       * @param isMutable Whether the referenced value is mutable.
       * @param type Type of declared variable. Null if no type declaration was given.
       * @param initExpr Initial value of variable. Never null.
       * @param position Position of AST element in source code. Never null.
       */
      public Let {
        requireNonNull(identifier);
        requireNonNull(initExpr);
        requireNonNull(position);
      }
    }

    /** Return statement, exiting from current function, returning value of given expression. */
    public record Return(Expression resultExpr, PosInterval position) implements Statement {
      /**
       * Create new return statement AST element.
       *
       * @param resultExpr Expression producing return value.
       * @param position Position of AST element in source code.
       */
      public Return {
        requireNonNull(resultExpr);
        requireNonNull(position);
      }
    }

    /** Expression statement, wrapping expression, and discarding result. */
    public record Expr(Expression expr, PosInterval position) implements Statement {
      /**
       * Create new expression statement AST element.
       *
       * @param expr Expression to execute.
       * @param position Position of AST element in source code.
       */
      public Expr {
        requireNonNull(expr);
        requireNonNull(position);
      }
    }
  }

  /** Unary operation. */
  public enum Unop {
    /** Produces the negative value of operand. Rust syntax: {@code -x} */
    NEGATE,
    /** Produces the bitwise not of the operand. Rust syntax: {@code !x} */
    BITWISE_NOT,
  }

  /** Binary operation. */
  public enum Binop {
    /** Multiplies operands together. Rust syntax: {@code x * y} */
    MULT,
    /** Divides left operand by right. Rust syntax: {@code x / y} */
    DIV,
    /** Computes remainder of left if divided by right. Rust syntax: {@code x % y} */
    REMAINDER,
    /** Adds operands together. Rust syntax: {@code x + y} */
    PLUS,
    /** Subtracts right operand from the left. Rust syntax: {@code x - y} */
    MINUS,
    /** Logical bitshift right. Rust syntax: {@code x >> y} */
    BITSHIFT_RIGHT,
    /** Logical bitshift left. Rust syntax: {@code x << y} */
    BITSHIFT_LEFT,
    /** Bitwise and between operands. Rust syntax: {@code x & y} */
    BITWISE_AND,
    /** Bitwise xor between operands. Rust syntax: {@code x ^ y} */
    BITWISE_XOR,
    /** Bitwise or between operands. Rust syntax: {@code x | y} */
    BITWISE_OR,
    /** Logical and between operands. Rust syntax: {@code x && y} */
    LOGICAL_AND,
    /** Logical or between operands. Rust syntax: {@code x || y} */
    LOGICAL_OR,

    /** Checks equality between operands. Rust syntax: {@code x == y} */
    EQUAL,
    /** Checks inequality between operands. Rust syntax: {@code x != y} */
    NOT_EQUAL,
    /** Checks less than relation between operands. Rust syntax: {@code x < y} */
    LESS_THAN,
    /** Checks less than or equal relation between operands. Rust syntax: {@code x <= y} */
    LESS_THAN_OR_EQUAL,
    /** Checks greater than relation between operands. Rust syntax: {@code x > y} */
    GREATER_THAN,
    /** Checks greater than or equal relation between operands. Rust syntax: {@code x >= y} */
    GREATER_THAN_OR_EQUAL,

    /** Indexes into left operand with value of right operand. Rust syntax: {@code x[y]} */
    INDEX,

    /** Creates range value from operands. Rust syntax: {@code x..y} */
    RANGE,
  }

  /** Indicates that the given AST element possess a source code position available. */
  public interface HasPosition {
    /**
     * Position of the AST element, covering the whole interval.
     *
     * @return Position of AST element in source code.
     */
    PosInterval position();
  }

  /** Position of a AST element within the source code. */
  public record PosInterval(
      int lineIdxStart, int columnIdxStart, int lineIdxEndExclusive, int columnIdxEndExclusive) {

    /** Represents an unknown source position. Avoid using this as much as possible. */
    static final PosInterval UNKNOWN = new PosInterval(-1, -1, -1, -1);
  }
}
