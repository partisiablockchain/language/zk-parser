fn do_something(x: u32) -> u32 {
    (x as i32) as u32
}
