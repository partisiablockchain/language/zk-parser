pub fn if_expr(x: i32) -> i32 {
    (if x != 3 { 1337 } else { 0 })
}
