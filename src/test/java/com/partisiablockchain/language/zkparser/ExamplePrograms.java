package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Stream;

final class ExamplePrograms {

  private ExamplePrograms() {}

  public static final List<String> TEST_CASES_SUCCESS_STABLE = filesInDirectory("tests_success");
  public static final List<String> TEST_CASES_SUCCESS_UNSTABLE =
      filesInDirectory("tests_success_unstable_pretty");
  public static final List<String> TEST_CASES_SUCCESS =
      Stream.concat(TEST_CASES_SUCCESS_STABLE.stream(), TEST_CASES_SUCCESS_UNSTABLE.stream())
          .toList();
  public static final List<String> TEST_CASES_UNSUPPORTED = filesInDirectory("tests_unsupported");
  public static final List<String> TEST_CASES_SYNTAXERROR = filesInDirectory("tests_syntaxerror");
  public static final List<String> TEST_CASES_WIP = filesInDirectory("tests_wip");

  private static List<String> filesInDirectory(final String directoryName) {
    final File directory = new File(ExamplePrograms.class.getResource(directoryName).getPath());
    final int prefixLength = directory.getParentFile().getAbsolutePath().length() + 1;
    return java.util.Arrays.stream(directory.listFiles())
        .map(File::getAbsolutePath)
        .filter(filename -> filename.endsWith(".rs"))
        .map(filename -> filename.substring(prefixLength))
        .toList();
  }

  public static String loadProgramText(final String filename) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> ExamplePrograms.class.getResourceAsStream(filename),
            InputStream::readAllBytes,
            "Could not load file: " + filename);

    return new String(fileBytes, StandardCharsets.UTF_8);
  }
}
