//! assert fail_parse "mismatched input '::' expecting"
use crate::pbc_zk::{
    other_submodule::{sbi32_from, sbi32_of},
    submodule::Sbi32,
};

fn identity(x: Sbi32) -> Sbi32 {
    x
}
