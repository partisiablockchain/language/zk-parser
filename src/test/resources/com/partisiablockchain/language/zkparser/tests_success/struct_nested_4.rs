struct Pair<V1, V2> {
    v1: V1,
    v2: V2,
}

struct MyStruct {
    my_pairs: Vec<Pair<V1, V2>>,
}
