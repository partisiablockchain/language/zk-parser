struct Range<#[some_attribute] T: PartialOrd> {
    low: T,
    high: T,
}
