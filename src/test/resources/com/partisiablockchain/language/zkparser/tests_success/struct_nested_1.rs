struct UsizeRange {
    low: usize,
    high: usize,
}

struct MyStruct {
    range1: UsizeRange,
    range2: UsizeRange,
}
