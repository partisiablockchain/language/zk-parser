/// Perform a zk computation on secret-shared data.
/// Find the winner of the second-price auction.
/// Finds the highest bidder and the amount of the second-highest bid
pub fn zk_compute(bids: Vec<SecretVar<Sbi32> >) -> (Sbi32, Sbi32) {
    let mut highest_bidder: Sbi32 = 0.into();
    let mut highest_amount: Sbi32 = 0.into();
    let mut second_highest_amount: Sbi32 = 0.into();
    for bid in bids {
        if bid.value > highest_amount {
            second_highest_amount = highest_amount;
            highest_amount = bid.value;
            highest_bidder = bid.metadata.into();
        } else if bid.value > second_highest_amount {
            second_highest_amount = bid.value;
        }
    }
    (highest_bidder, second_highest_amount)
}
