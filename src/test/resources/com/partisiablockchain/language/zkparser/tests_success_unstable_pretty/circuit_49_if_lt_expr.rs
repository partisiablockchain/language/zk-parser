pub fn if_expr(x: i32) -> i32 {
    (if 3 < x { 1337 } else { 0 })
}
