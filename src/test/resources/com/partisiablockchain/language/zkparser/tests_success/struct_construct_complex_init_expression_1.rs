struct MyStruct {
    field1: u32,
    field2: u32,
}

fn func() -> MyStruct {
    let v1: MyStruct = MyStruct { field1: 1, field2: 2 };
    let v2: MyStruct = MyStruct { field1: 3, field2: 4 };
    MyStruct { field1: v1.field1 + v1.field2, field2: v2.field2 + v2.field2 }
}
