package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkparser.parser.RustZkLexer;
import com.partisiablockchain.language.zkparser.parser.RustZkParser;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * Parser for the Partisia ZK subset of Rust.
 *
 * <p>Should be the inverse of {@link ZkAstPretty}.
 */
public final class RustZk {

  private RustZk() {}

  /**
   * Parse a ZK Rust source to ZkAst.
   *
   * @param rustZkSource The source code as a string
   * @param sourceName The (file) name of source. Used for errors.
   * @return The parsed source file AST
   */
  public static ParseResult parse(final String rustZkSource, final String sourceName) {
    // Create parser
    final SyntaxErrorListener errorListener = new SyntaxErrorListener();
    final RustZkParser parser = createParser(rustZkSource, sourceName, errorListener);

    // Actually parse
    final RustZkParser.FileContext fileContext = parser.file();

    // Check for parsing errors
    if (!errorListener.syntaxErrors().isEmpty()) {
      return new ParseResult(null, List.copyOf(errorListener.syntaxErrors()));
    }

    // Convert to strict AST
    final ParseResult parseResult = ZkAstConverter.convertProgram(fileContext);
    return new ParseResult(
        parseResult.syntaxErrors().isEmpty() ? parseResult.program() : null,
        parseResult.syntaxErrors());
  }

  static RustZkParser createParser(
      String rustZkSource, String sourceName, BaseErrorListener errorListener) {
    // Create parser
    final RustZkLexer lexer = new RustZkLexer(CharStreams.fromString(rustZkSource, sourceName));
    final RustZkParser parser = new RustZkParser(new CommonTokenStream(lexer));
    parser.removeErrorListeners();
    parser.addErrorListener(errorListener);
    return parser;
  }

  /**
   * Internal utility for parsing or throwing.
   *
   * @param rustZkSource The source code as a string
   * @param sourceName The (file) name of source. Used for errors.
   * @return The parsed source file AST
   */
  static ZkAst.Program parseOrThrow(final String rustZkSource, final String sourceName) {
    final ParseResult result = parse(rustZkSource, sourceName);
    if (!result.syntaxErrors().isEmpty()) {
      final String errorLines =
          result.syntaxErrors().stream()
              .map(
                  e ->
                      "  %s:%d:%d-%d: %s"
                          .formatted(
                              sourceName,
                              e.position().lineIdxStart(),
                              e.position().columnIdxStart(),
                              e.position().columnIdxEndExclusive(),
                              e.message()))
              .collect(Collectors.joining("\n"));
      throw new RuntimeException("Could not parse:\n" + errorLines);
    }
    return result.program();
  }

  /**
   * Result type for {@link parse}.
   *
   * <p>Contains a parsed program if successfully parsed, and a list of syntax errors if not parsed.
   */
  public record ParseResult(ZkAst.Program program, List<ErrorLine> syntaxErrors) {}

  /**
   * Throws exception with a descriptive message on any syntax errors.
   *
   * <p>Format the statement source in <a
   * href="https://www.gnu.org/prep/standards/html_node/Errors.html">GCC format</a>
   */
  static final class SyntaxErrorListener extends BaseErrorListener {

    private final List<ErrorLine> syntaxErrors = new ArrayList<>();

    public List<ErrorLine> syntaxErrors() {
      return this.syntaxErrors;
    }

    @Override
    public void syntaxError(
        Recognizer<?, ?> recognizer,
        Object offendingSymbol,
        int line,
        int charPositionInLine,
        String msg,
        RecognitionException e) {
      int widthOfSymbol = 1;
      if (e != null) {
        widthOfSymbol = e.getOffendingToken().getText().length();
      }
      syntaxErrors.add(
          new ErrorLine(
              msg,
              new ZkAst.PosInterval(
                  line, charPositionInLine, line, charPositionInLine + widthOfSymbol)));
    }
  }

  /** Thrown by the parser when a syntax error occurs. */
  public record ErrorLine(String message, ZkAst.PosInterval position) {}
}
