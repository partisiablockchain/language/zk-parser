package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.immutabledetect.ImmutableDetect;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/** Test the RustZk parser and pretty-printer. */
public final class RustZkRoundTripTest {

  static Stream<Arguments> provideTestCasesSuccessUnstable() {
    return ExamplePrograms.TEST_CASES_SUCCESS_UNSTABLE.stream().map(Arguments::of);
  }

  static Stream<Arguments> provideTestCasesSuccessStable() {
    return ExamplePrograms.TEST_CASES_SUCCESS_STABLE.stream().map(Arguments::of);
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesSuccessUnstable")
  void testZkRustFile(final String filename) {
    testZkRustFileInternal(filename, false);
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesSuccessStable")
  void testPrettySameAsInput(final String filename) {
    testZkRustFileInternal(filename, true);
  }

  private void testZkRustFileInternal(final String filename, boolean expectIdentical) {
    final String source = ExamplePrograms.loadProgramText(filename);
    final String normalizedSource = source.strip().replace("\r\n", "\n");

    // Parse to AST
    final ZkAst.Program zkAstFirst =
        ExceptionConverter.call(() -> RustZk.parseOrThrow(source, filename), filename);
    Assertions.assertThat(zkAstFirst).matches(ImmutableDetect::isImmutable, "is immutable");

    // AST to pretty string
    final String prettySource =
        ExceptionConverter.call(() -> ZkAstPretty.pretty(zkAstFirst), filename)
            .replace("\r\n", "\n");
    Assertions.assertThat(prettySource).as(filename).isNotBlank();
    if (expectIdentical) {
      Assertions.assertThat(prettySource).as(filename).isEqualTo(normalizedSource);
    } else {
      Assertions.assertThat(prettySource).as(filename).isNotEqualTo(normalizedSource);
    }

    // Parse pretty string back to AST.
    final ZkAst.Program zkAstSecond =
        ExceptionConverter.call(() -> RustZk.parseOrThrow(prettySource, filename), filename);
    Assertions.assertThat(zkAstSecond).matches(ImmutableDetect::isImmutable, "is immutable");
    Assertions.assertThat(normalizePositions(zkAstSecond))
        .as(filename)
        .isEqualTo(normalizePositions(zkAstFirst));
  }

  private static ZkAst.Program normalizePositions(final ZkAst.Program program) {
    return RecursiveStructureMapper.map(
        program, ZkAst.PosInterval.class, x -> ZkAst.PosInterval.UNKNOWN);
  }
}
