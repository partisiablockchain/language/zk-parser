struct IdenticalPair<V: Sized> {
    v1: V,
    v2: V,
}
