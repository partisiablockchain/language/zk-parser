struct Pair<V1, V2> {
    v1: V1,
    v2: V2,
}

// Ensure that we still support nested generics ending with "> >"
struct MyStruct {
    my_pairs: Vec<Pair<V1, V2> >,
}
