struct Range<T: PartialOrd> {
    low: T,
    high: T,
}

struct MyStruct {
    rangeUsize: Range<usize>,
    rangeU32: Range<u32>,
}
