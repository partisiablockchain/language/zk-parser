// Fails because && is separated by a line
pub fn add_self(x: i32) -> i32 {
    x &
       & 4
}
