package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test the RustZk parser. */
public final class RustZkPrettifyTest {

  @Test
  void testParseFunction() {
    final String source =
        """
        fn compute() -> i32 {
            return v1+v2+v3;
        }
        """;
    final String expected =
        """
        fn compute() -> i32 {
            return (v1 + v2) + v3;
        }
        """;
    assertParseAndStringify(source, expected);
  }

  @Test
  void testParseFunctionWithLoop() {
    final String source =
        """
        fn sum(salaries: &[i32]) -> i32 {
            let mut sum: i32  = 0;
            for i in 0..salaries.len() {
                sum = sum + salaries[i];
            }
            sum
        }
        """;
    final String expected =
        """
        fn sum(salaries: &[i32]) -> i32 {
            let mut sum: i32 = 0;
            for i in 0 .. salaries.len() {
                sum = (sum + salaries[i]);
            }
            sum
        }
        """;
    assertParseAndStringify(source, expected);
  }

  @Test
  void testParseIfs() {
    final String source =
        """
        fn compute() {
            if a == 0 {
                a = 1;
            }
            if b == 0 {
                b = 1;
            } else {
                b = 2;
            }
            if c == 0 {
                c = 1;
            } else if c == 1 {
                c = 2;
            } else {
                c = 3;
            }
        }
        """;
    final String expected =
        """
        fn compute() {
            if a == 0 {
                a = 1;
            }
            if b == 0 {
                b = 1;
            } else {
                b = 2;
            }
            (if c == 0 {
                c = 1;
            } else if c == 1 {
                c = 2;
            } else {
                c = 3;
            })
        }
        """;
    assertParseAndStringify(source, expected);
  }

  @Test
  void testParseIfExpression() {
    final String source =
        """
        fn compute() {
          let a: i32 = if b==0 { 1 } else { 2 };
        }
        """;
    final String expected =
        """
        fn compute() {
            let a: i32 = if b == 0 {
                1
            } else {
                2
            };
        }
        """;
    assertParseAndStringify(source, expected);
  }

  @Test
  void testParseTypes() {
    final String source =
        """
        fn types(
        u : usize,
        i : i32,
        a : [usize;SIZE],
        r : &usize,
        m : & mut i32,
        s : &[i32],
        ) -> i32 { 1 }
        """;
    final String expected =
        """
        fn types(u: usize, i: i32, a: [usize; SIZE], r: &usize, m: &mut i32, s: &[i32]) -> i32 {
            1
        }
        """;
    assertParseAndStringify(source, expected);
  }

  @Test
  void testParsePrecedence() {
    final String source =
        """
        fn compute() {
          (1+v1)+v2*v3+v5*-v6
        }
        """;
    final String expected =
        """
        fn compute() {
            ((1 + v1) + (v2 * v3)) + (v5 * (-v6))
        }
        """;
    assertParseAndStringify(source, expected);
  }

  @Test
  void testParsePrecedence2() {
    final String source =
        """
        fn compute() {
          -a*b+c<<d&e^f|g==h&&i||j
        }
        """;
    final String expected =
        """
        fn compute() {
            (((((((((-a) * b) + c) << d) & e) ^ f) | g) == h) && i) || j
        }
        """;
    assertParseAndStringify(source, expected);
  }

  @Test
  void testSyntaxError() {
    final String source =
        """
        fn compute() {
          x x
        }
        """;
    Assertions.assertThatThrownBy(() -> RustZk.parseOrThrow(source, "myfile.rs"))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("myfile.rs:2:4-5: extraneous input 'x' expecting '}'");
  }

  private void assertParseAndStringify(final String source, final String expected) {
    final ZkAst.Program zkProgram = RustZk.parseOrThrow(source.strip(), "<inline>");
    final String prettified = ZkAstPretty.pretty(zkProgram);
    Assertions.assertThat(prettified).isEqualTo(expected.strip());
  }
}
