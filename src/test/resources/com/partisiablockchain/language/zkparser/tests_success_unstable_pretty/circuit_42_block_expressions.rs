fn nested_blocks(x: i32) -> i32 {
    2 + {
        let y: i32 = 3;
        { y + 2 } + { 2 + x }
    }
}
