package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThatCode;

import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Test precedence in the RustZk parser. */
public final class AstPrecedenceTest {

  /**
   * Provides test expressions for precedence testing. Parenthesis are explicit around the
   * expression operation with higher precedence.
   *
   * @return List of precedence test cases
   */
  static List<String> provideTestCasesPrecedence() {
    return List.of(
        // Method calls higher than Unary operators
        "-{a.method()}",
        "!{a.method()}",

        // Field and tuple indexing expressions higher than Unary operators
        "-{a.field}",
        "!{a.1}",

        // Array indexing higher than Unary operators
        "-{a[i]}",
        "!{a[i]}",

        // Function call higher than Unary operators
        "-{f()}",
        "!{f()}",

        // Unary operators higher than as
        "{-a} as i32",
        "{!a} as i32",

        // as higher than *, /, %
        "a * {b as i32}",
        "a / {b as i32}",
        "a % {b as i32}",

        // * higher than binary +, -
        "a + {b * 3}",
        "a - {b * 3}",

        // + higher than <<, >>
        "a << {b + 3}",
        "a >> {b + 3}",

        // << higher than &
        "a & {b << 3}",

        // & higher than ^
        "a ^ {b & 3}",

        // ^ higher than |
        "a | {b ^ 3}",

        // | higher than ==, !=, <, >, <=, >=
        "a == {b | 3}",
        "a != {b | 3}",
        "a < {b | 3}",
        "a > {b | 3}",
        "a <= {b | 3}",
        "a >= {b | 3}",

        // == higher than &&
        "a && {b == 3}",

        // && higher than ||
        "a || {b && 3}",

        // || higher than .., ..=
        "a..{b || 3}",

        // .. higher than =, +=, -=, *=, /*, %=, &=, |=, ^=. <<=, >>=
        "a = {b..3}",

        // Field expression and tuple index same precedence
        "{a.field}.1",
        "{a.1}.field",

        // Function call, array index same precedence
        "{f(})[1]",
        "{f[1](})",

        // Unary -, *, !, &, &mut same precedence
        "!{-a}",
        "-{!a}",

        // Binary *, /, % same precedence
        "{a * b} / 3",
        "{a / b} * 3",
        "{a * b} % 3",
        "{a % b} * 3",

        // Binary +, - same precedence
        "{a + b} - 3",
        "{a - b} + 3",

        // Binary <<, >> same precedence
        "{a << b} >> 3",
        "{a >> b} << 3",

        // Binary ==, !=, <, >, <=, >= same precedence
        "{a == b} != 3",
        "{a != b} == 3",
        "{a == b} < 3",
        "{a < b} == 3",
        "{a == b} > 3",
        "{a > b} == 3",
        "{a == b} <= 3",
        "{a <= b} == 3",
        "{a == b} >= 3",
        "{a >= b} == 3",

        // Binary .., ..= same precedence

        // Binary =, +=, -=, *=, /=, %=, &=, |=, ^=, <<=, >>= same precedence

        // as higher than assign
        "a = {b as i64}",

        // Associativity
        "a = {(b as i32} as i64)",
        "{a + b} + c");
  }

  static List<String> provideUnimplementedTestcases() {
    return List.of(
        // Unary operators higher than as
        "{*a} as i32",
        "{&a} as i32",
        "{&mut a} as i32",

        // || higher than .., ..=
        "a..={b || 3}",

        // .. higher than =, +=, -=, *=, /*, %=, &=, |=, ^=. <<=, >>=
        "a += {b..3}",
        "a -= {b..3}",
        "a *= {b..3}",
        "a /= {b..3}",
        "a %= {b..3}",
        "a &= {b..3}",
        "a |= {b..3}",
        "a ^= {b..3}",
        "a <<= {b..3}",
        "a >>= {b..3}",

        // Unary -, *, !, &, &mut same precedence
        "*{-a}",
        "-{*a}",
        "&{-a}",
        "-{&a}",
        "&mut {-a}",
        "-{&mut a}",

        // Binary .., ..= same precedence
        "{a..b}..=3",
        "{a..=b}..3",

        // Binary =, +=, -=, *=, /=, %=, &=, |=, ^=, <<=, >>= same precedence
        "{a = b} += 3",
        "{a += b} = 3",
        "{a = b} -= 3",
        "{a -= b} = 3",
        "{a = b} *= 3",
        "{a *= b} = 3",
        "{a = b} /= 3",
        "{a /= b} = 3",
        "{a = b} %= 3",
        "{a %= b} = 3",
        "{a = b} &= 3",
        "{a &= b} = 3",
        "{a = b} |= 3",
        "{a |= b} = 3",
        "{a = b} ^= 3",
        "{a ^= b} = 3",
        "{a = b} <<= 3",
        "{a <<= b} = 3",
        "{a = b} >>= 3",
        "{a >>= b} = 3");
  }

  /**
   * Tests correct precedence when parsing a ZkRust expression. The expression must be given with
   * curly braces denoting the correct precedence. Checks that the AST generated by parsing the
   * expression with and without parenthesis are equal.
   *
   * @param expr String representation of the expression.
   */
  @ParameterizedTest
  @MethodSource("provideTestCasesPrecedence")
  void testPrecedence(final String expr) {
    final String exprPrecedence = expr.replace("{", "(").replace("}", ")");
    final String exprNoParenthesis = expr.replace("{", " ").replace("}", " ");

    ZkAst.Expression expectedExpression = AstConverterTest.parseExpression(exprPrecedence);
    ZkAst.Expression precedenceExpression = AstConverterTest.parseExpression(exprNoParenthesis);

    Assertions.assertThat(normalizePositions(expectedExpression))
        .isEqualTo(normalizePositions(precedenceExpression));
  }

  /**
   * Tests that a given expression fails to parse, because the used operator is not implemented.
   *
   * @param expr String representing the expression.
   */
  @ParameterizedTest
  @MethodSource("provideUnimplementedTestcases")
  void testTodo(final String expr) {
    final String cleanExpression = expr.replace("{", "(").replace("}", ")");
    assertThatCode(() -> AstConverterTest.parseExpression(cleanExpression))
        .isInstanceOf(AssertionError.class);
  }

  private static ZkAst.Expression normalizePositions(final ZkAst.Expression expr) {
    return RecursiveStructureMapper.map(
        expr, ZkAst.PosInterval.class, x -> ZkAst.PosInterval.UNKNOWN);
  }
}
