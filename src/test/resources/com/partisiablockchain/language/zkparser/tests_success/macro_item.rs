pub fn circuit_1(x: i32) -> i32 {
    x + 9
}

pbc_zk::test_eq!(circuit_1(3), 12);
pbc_zk::test_eq!(circuit_1(8), 17, [1i32, 2i32, 3i32]);
pbc_zk::test_eq!(circuit_1(8), 17, [], [1i32, 2i32, 3i32]);
pbc_zk::test_eq!(circuit_1(8), 17, [1i32], [1i32]);
some::other::macro_invocation!("Hello", "World");
assert_eq!(2, "two");
print_ln!("Testing {}", 42);
