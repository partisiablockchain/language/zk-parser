pub fn if_expression(x: i32) -> i32 {
    let x_squared: i32 = x * x;
    if x_squared < 15 {
        2
    } else {
        3
    }
}
