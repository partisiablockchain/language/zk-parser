pub fn bool_and(a: bool, b: bool) -> bool {
    a == b
}
