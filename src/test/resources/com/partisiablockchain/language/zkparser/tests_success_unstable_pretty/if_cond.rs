pub fn do_nothing_useful(y: [i32; 1]) -> i32 {
    if y {
        1
    } else {
        2
    }
}
