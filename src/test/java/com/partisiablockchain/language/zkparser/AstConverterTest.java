package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigInteger;
import org.junit.jupiter.api.Test;

final class AstConverterTest {

  @Test
  public void parseExpressions() {
    assertThat(parseExpression("x[2]"))
        .isEqualTo(
            new ZkAst.Expression.Binary(
                ZkAst.Binop.INDEX,
                new ZkAst.Expression.Variable(
                    ZkAst.Path.simple(new ZkAst.PosInterval(1, 20, 1, 21), "x"),
                    new ZkAst.PosInterval(1, 20, 1, 21)),
                new ZkAst.Expression.IntLiteral(
                    BigInteger.TWO, null, new ZkAst.PosInterval(1, 22, 1, 23)),
                new ZkAst.PosInterval(1, 20, 1, 24)));
  }

  /**
   * Parses a string representing a ZkRust expression into ZkAst Expression.
   *
   * @param expr String representation of ZkRust expression.
   * @return ZkAst Expression representation of the input String.
   */
  public static ZkAst.Expression parseExpression(final String expr) {
    final RustZk.ParseResult parseResult =
        RustZk.parse("pub fn f() { return " + expr + "; }", "<inline>");
    assertThat(parseResult).isNotNull();

    final ZkAst.Program programAst = parseResult.program();
    assertThat(programAst).isNotNull();

    return ((ZkAst.Statement.Return)
            ((ZkAst.Expression.Block) programAst.functions().get(0).item().body())
                .statements()
                .get(0))
        .resultExpr();
  }
}
