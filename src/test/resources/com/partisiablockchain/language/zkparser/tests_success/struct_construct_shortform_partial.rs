struct MyStruct {
    field1: u32,
    field2: u32,
}

fn func() -> MyStruct {
    let field1 = 1;
    let v2 = 2;
    MyStruct { field1, field2: v2 }
}
