struct Range<T: PartialOrd> {
    #[some_attribute]
    low: T,
    high: T,
}

