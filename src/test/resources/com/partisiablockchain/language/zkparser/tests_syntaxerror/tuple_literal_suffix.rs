//! assert fail_parse "no viable alternative at input '(1,2,3).2i64'"
pub fn tuple_literal_suffix() -> i32 {
    (1, 2, 3).2i64
}
