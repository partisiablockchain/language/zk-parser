struct MyStruct {
    field1: u32,
    field2: u32,
}

struct OtherStruct {
    a: u32,
    b: u32,
}

fn func() -> MyStruct {
    let v1 = OtherStruct { a: 1, b: 2 };
    MyStruct { field1: 1 + v1.a, field2: 2 + v1.b }
}
