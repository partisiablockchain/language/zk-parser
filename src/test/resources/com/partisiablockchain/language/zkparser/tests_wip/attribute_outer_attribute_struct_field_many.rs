struct Range<T: PartialOrd> {
    #[some_attribute] #[some_other_attribute]
    low: T,
    high: T,
}
