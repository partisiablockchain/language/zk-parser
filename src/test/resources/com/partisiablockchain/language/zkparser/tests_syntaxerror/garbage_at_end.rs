//! assert fail_parse "mismatched input 'ijsfeoi' expecting {'!', '::'}"
//! assert eval(x) => 15
pub fn circuit_1(x: Sbi32) -> Sbi32 {
    if x == sbi32_from(9) {
        sbi32_from(15)
    } else {
        sbi32_from(15)
    }
}

isjefoijsfe ijsfeoi jsefoi jsoiefjsiejfosiejf oisjefoi jsefoi
