struct Internal {
    v1: u32,
    v2: u32,
}

struct Outer {
    field1: Internal,
    field2: u32,
}

fn func() -> Outer {
    Outer { field1: Inner { v1: 1, v2: 2 }, field2: 3 }
}
