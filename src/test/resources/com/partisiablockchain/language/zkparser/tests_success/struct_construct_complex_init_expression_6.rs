struct MyStruct {
    field1: u32,
    field2: u32,
}

struct OtherStruct {
    a: u32,
    b: u32,
}

fn func() -> MyStruct {
    let field1 = OtherStruct { a: 1, b: 2 };
    MyStruct { field1: field1.a, field2: field1.b }
}
