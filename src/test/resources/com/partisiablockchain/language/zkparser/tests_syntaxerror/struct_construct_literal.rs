//! assert fail_parse "no viable alternative at input 'MyStruct{4'"

struct MyStruct {
    f1: i32,
}

pub fn construct() -> MyStruct {
    MyStruct { 4 }
}
