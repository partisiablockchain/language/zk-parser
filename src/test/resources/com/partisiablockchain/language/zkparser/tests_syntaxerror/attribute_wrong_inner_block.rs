//! assert fail_parse "no viable alternative at input '#[some_block_attribute]0'"
fn compute() -> i32 {
    #[some_block_attribute]
    0
}
