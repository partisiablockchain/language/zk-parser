struct MyStruct {
    field1: u32,
    field2: u32,
}

fn func() -> u32 {
    MyStruct { field1: 1, field2: 2 }.field1
}
