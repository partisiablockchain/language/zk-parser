//! assert fail_parse "no viable alternative at input '#[some_block_attribute]#[some_block_attribute_2]#[some_block_attribute_3]#[some_block_attribute_4]0'"
fn compute() -> i32 {
    #[some_block_attribute]
    #[some_block_attribute_2]
    #[some_block_attribute_3]
    #[some_block_attribute_4]
    0
}
