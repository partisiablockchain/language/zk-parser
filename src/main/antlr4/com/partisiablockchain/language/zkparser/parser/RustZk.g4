// Rust Zero Knowledge Parser
// A small subset of RUST that can be compiled to execute Secure Multiparty Computations for the Partisia Blockchain
// Based on https://doc.rust-lang.org/reference/introduction.html

grammar RustZk;

file
    : item* EOF
    ;

item
    : programItem
    | macroInvocationSemi
    ;

programItem
    : outerAttribute* visibility? visItem
    ;

visibility
    : 'pub' #visibilityPub
    | 'pub' PAR_LEFT CRATE PAR_RIGHT #visibilityCrate
    ;

visItem
    : function
    | constantItem
    | struct
    | useDeclaration
    | typeAliasItem
    | module
    ;

// https://doc.rust-lang.org/stable/reference/macros.html#macro-invocation
macroInvocationSemi
    : macroInvocation ';'
    ;

// https://doc.rust-lang.org/reference/attributes.html
outerAttribute
    : '#' BRACKET_LEFT attr BRACKET_RIGHT
    ;

// https://doc.rust-lang.org/reference/attributes.html
innerAttribute
    : '#' '!' BRACKET_LEFT attr BRACKET_RIGHT
    ;

// https://doc.rust-lang.org/reference/items/constant-items.html
constantItem
    : CONST IDENTIFIER ':' type ASSIGN expression SEMICOLON
    ;

// https://doc.rust-lang.org/reference/items/type-aliases.html
typeAliasItem
    : 'type' IDENTIFIER ASSIGN type SEMICOLON
    ;

// https://doc.rust-lang.org/reference/items/functions.html
function
    : FN IDENTIFIER PAR_LEFT functionParameters? PAR_RIGHT functionReturnType? ( blockExpr | SEMICOLON )
    ;

functionParameters
    : functionParam ( ',' functionParam )* ','?
    ;

functionParam
    : outerAttribute* mut? IDENTIFIER ':' type
    ;

functionReturnType
    : '->' type
    ;

// https://doc.rust-lang.org/reference/items/structs.html
struct
    : STRUCT IDENTIFIER genericParams CURLY_LEFT structFields CURLY_RIGHT
    ;

structFields
    :
    | structField ( ',' structField )* ','?
    ;

structField
    : outerAttribute* visibility? IDENTIFIER ':' type
    ;

// https://doc.rust-lang.org/reference/items/generics.html
genericParams
    :
    | LT GT
    | LT genericParam ( ',' genericParam )* ','? GT
    ;

genericParam
    : outerAttribute* genericParamType
    ;

genericParamType
    : IDENTIFIER
    | IDENTIFIER ':' IDENTIFIER
    ;

type
    : BRACKET_LEFT type (SEMICOLON expression)? BRACKET_RIGHT #typeArray
    | PAR_LEFT  PAR_RIGHT #typeUnit
    | PAR_LEFT type? ( ',' type )*  ','? PAR_RIGHT #typeTuple
    | AND mut? type #typeReference
    | pathInType #typeNamed
    ;

pathInType
    : simplePath ( LT pathGenerics GT )?
    ;

pathGenerics
    :   type? ( ',' type )* ','?
    ;

// https://doc.rust-lang.org/reference/expressions/block-expr.html
blockExpr
    : CURLY_LEFT innerAttribute* statements CURLY_RIGHT
    ;

// https://doc.rust-lang.org/reference/statements.html
statements
    : statement* expression #statementsWithExpr
    | statement* #statementsWithoutExpr
    ;

// https://doc.rust-lang.org/reference/statements.html
statement
    :  SEMICOLON #emptyStatement
    |  outerAttribute* LET mut? IDENTIFIER ( ':' type )? ASSIGN expression SEMICOLON #letStatement
    |  forExpression #forStatement
    |  ifExpression #ifStatement
    |  RETURN expression SEMICOLON #returnStatement
    |  expression SEMICOLON #exprStatement
    ;

mut
  : 'mut'
  ;

// https://doc.rust-lang.org/reference/expressions.html
expression
    : blockExpr #exprBlock
    | PAR_LEFT expression PAR_RIGHT #exprPar
    | PAR_LEFT expression ( ',' expression )* ','? PAR_RIGHT #exprTuple
    | PAR_LEFT PAR_RIGHT #exprUnit
    | structExpression #exprStructConstructor
    | ifExpression #exprIf
    | forExpression #exprFor
    | expression DOT IDENTIFIER PAR_LEFT callValueArguments PAR_RIGHT #exprMethodCall
    | expression DOT IDENTIFIER #exprField
    | expression DOT INT_LITERAL_DEC #exprTupleIndex
    | expression PAR_LEFT callValueArguments PAR_RIGHT #exprCall
    | expression BRACKET_LEFT expression BRACKET_RIGHT #exprArrayIndex
    | BRACKET_LEFT arrayElements BRACKET_RIGHT #exprArrayConstructor
    | ( '-' | '!' ) expression #exprUnary
    | expression AS type #exprTypeCast
    | expression ( STAR | '/' | '%' )expression #exprBinary
    | expression ( '+' | '-' ) expression #exprBinary
    | expression ( LT LT | GT GT ) expression #exprBinary
    | expression AND expression #exprBinary
    | expression '^' expression #exprBinary
    | expression OR expression #exprBinary
    | expression ( '==' | '!=' | LT |GT  | '<=' | '>=' ) expression #exprBinary
    | expression AND AND expression #exprBinary
    | expression OR OR expression #exprBinary
    | expression '..' expression #exprBinary
    | expression ASSIGN expression #exprAssign
    | macroInvocation #exprMacroInvokation
    | pathInExpression #exprIdentifier
    | intLiteral   #exprLiteralInt
    | BOOL         #exprLiteralBool
    | STRING       #exprLiteralString
    | BREAK        #exprBreak
    | CONTINUE     #exprContinue
    ;

// https://doc.rust-lang.org/reference/expressions/array-expr.html#array-expressions
arrayElements
    : ( expression ( ',' expression )* ','? )? # arrayElementsExplicit
    | expression SEMICOLON expression # arrayElementsRepeated
    ;

// https://doc.rust-lang.org/reference/expressions/struct-expr.html
structExpression
    : pathInExpression CURLY_LEFT structExpressionFields CURLY_RIGHT
    ;

structExpressionFields
    :
    | structExpressionField ( ',' structExpressionField )* ','?
    ;

structExpressionField
    : IDENTIFIER ':' expression
    | IDENTIFIER
    ;

// https://doc.rust-lang.org/reference/expressions/if-expr.html#if-expressions
ifExpression
    : IF expression blockExpr ( 'else' ( blockExpr | ifExpression ) )?
    ;

forExpression
    : FOR IDENTIFIER 'in' expression blockExpr
    ;

callValueArguments
    :   expression? ( ',' expression )* ','?
    ;

pathInExpression
    : simplePath ( DCOLON LT pathGenerics GT )?
    ;

// https://doc.rust-lang.org/reference/macros.html?highlight=MacroInvocation#macro-invocation
macroInvocation
    :   simplePath '!' PAR_LEFT callValueArguments PAR_RIGHT
    ;

useDeclaration
    : USE useTree SEMICOLON
    ;

useTree
    : simplePath DCOLON STAR
    | simplePath DCOLON CURLY_LEFT ( IDENTIFIER ( ',' IDENTIFIER )* ','? )? CURLY_RIGHT
    | simplePath
    ;

simplePath
    : simplePathSegment (DCOLON simplePathSegment)*
    ;

simplePathSegment
    : IDENTIFIER
    | CRATE
    ;

// There are no negative integers
// https://doc.rust-lang.org/reference/tokens.html#integer-literals
intLiteral
    : INT_LITERAL_DEC | INT_LITERAL_BIN | INT_LITERAL_OCT | INT_LITERAL_HEX
    | INT_LITERAL_DEC_W_SUFFIX | INT_LITERAL_BIN_W_SUFFIX | INT_LITERAL_OCT_W_SUFFIX | INT_LITERAL_HEX_W_SUFFIX
    ;

attr
    : metaItem
    ;

// https://doc.rust-lang.org/reference/attributes.html#meta-item-attribute-syntax
metaItem
    : simplePath #metaItemName
    | simplePath ASSIGN expression #metaItemAssign
    | simplePath PAR_LEFT ( metaItemInner ( ',' metaItemInner )* ','? )?  PAR_RIGHT #metaItemCall
    ;

metaItemInner
    : metaItem
    | expression
    ;

module
    : MOD IDENTIFIER SEMICOLON ;

MOD : 'mod';
CRATE : 'crate';
DCOLON : '::';
SEMICOLON : ';';
RETURN : 'return';
LET : 'let';
IF : 'if';
FOR : 'for';
ASSIGN : '=';
CONST : 'const';
STRUCT : 'struct';
FN : 'fn';
PAR_LEFT : '(';
PAR_RIGHT : ')';
CURLY_LEFT : '{';
CURLY_RIGHT : '}';
BRACKET_LEFT : '[';
BRACKET_RIGHT : ']';
DOT : '.';
AS : 'as';
STAR : '*';
USE : 'use';
LT : '<';
GT : '>';
AND : '&' ;
OR : '|' ;
CONTINUE : 'continue';
BREAK : 'break';
WHILE : 'while';

BOOL : 'true' | 'false';
STRING : '"' .*? '"' ;
IDENTIFIER : [_a-zA-Z][_a-zA-Z0-9]* ;
WS : [ \t\r\n]+ -> skip ;
COMMENT_LINE: '//' ~[\r\n]* -> skip;
BLOCK_COMMENT: '/*' (BLOCK_COMMENT | . )*? '*/' -> skip;

// Integer rules adapted from <https://doc.rust-lang.org/reference/tokens.html#integer-literals>
INT_LITERAL_DEC : INT_DIGIT_DEC ( INT_DIGIT_DEC | '_' )*;
INT_LITERAL_DEC_W_SUFFIX : INT_LITERAL_DEC IDENTIFIER;
INT_LITERAL_BIN : '0b' ( INT_DIGIT_BIN | '_' )* INT_DIGIT_BIN ( INT_DIGIT_BIN | '_' )*;
INT_LITERAL_BIN_W_SUFFIX : INT_LITERAL_BIN IDENTIFIER;
INT_LITERAL_OCT : '0o' ( INT_DIGIT_OCT | '_' )* INT_DIGIT_OCT ( INT_DIGIT_OCT | '_' )*;
INT_LITERAL_OCT_W_SUFFIX : INT_LITERAL_OCT IDENTIFIER;
INT_LITERAL_HEX : '0x' ( INT_DIGIT_HEX | '_' )* INT_DIGIT_HEX ( INT_DIGIT_HEX | '_' )*;
INT_LITERAL_HEX_W_SUFFIX : INT_LITERAL_HEX IDENTIFIER;

INT_DIGIT_BIN : [0-1];
INT_DIGIT_OCT : [0-7];
INT_DIGIT_DEC : [0-9];
INT_DIGIT_HEX : [0-9a-fA-F];
