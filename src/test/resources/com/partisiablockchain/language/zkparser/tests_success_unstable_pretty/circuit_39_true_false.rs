pub fn circuit_1(x: i32) -> i32 {
    (if true { 1 } else { 3 }) + (if false { 10 } else { 30 })
}
