package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * Pretty-prints ZkAst programs.
 *
 * <p>Should be the inverse of {@link RustZk}.
 */
public final class ZkAstPretty {

  private ZkAstPretty() {}

  record PrettyBuilder(StringBuilder output, int indent, boolean withinExpression) {
    @CanIgnoreReturnValue
    PrettyBuilder append(final String text) {
      output().append(text);
      return this;
    }

    @CanIgnoreReturnValue
    PrettyBuilder appendNewline() {
      output.append("\n");
      output.append(" ".repeat(indent));
      return this;
    }

    PrettyBuilder withIndent() {
      return new PrettyBuilder(output, indent + 4, withinExpression);
    }

    PrettyBuilder withWithinExpression(final boolean newWithinExpression) {
      return new PrettyBuilder(output, indent, newWithinExpression);
    }
  }

  /**
   * Pretty-prints ZkAst program.
   *
   * <p>Should be parsable by {@link RustZk} into an identical program AST.
   *
   * @param program Program to pretty-print.
   * @return prettified string version of the given program.
   */
  public static String pretty(final ZkAst.Program program) {
    final var output = new PrettyBuilder(new StringBuilder(), 0, false);
    prettyProgram(program, output);
    return output.output().toString();
  }

  private static void prettyProgram(final ZkAst.Program program, final PrettyBuilder output) {
    int itemIndex = 0;

    // Items in program
    for (final ZkAst.ProgramItem<? extends ZkAst.ProgramItemData> itemWrapped : allItems(program)) {
      if (itemIndex++ > 0) {
        output.append(itemWrapped.item() instanceof ZkAst.UseDeclaration ? "\n" : "\n\n");
      }
      prettyItem(itemWrapped, output);
    }

    // Macro items
    if (program.macroItems().size() != 0) {
      output.append("\n");
    }
    for (final ZkAst.Expression.MacroInvocation macroItem : program.macroItems()) {
      output.append("\n");
      prettyExpression(macroItem, output);
      output.append(";");
    }
  }

  /**
   * Returns a single list containing all program items.
   *
   * @return All program items in a single list.
   */
  private static Iterable<ZkAst.ProgramItem<? extends ZkAst.ProgramItemData>> allItems(
      ZkAst.Program program) {
    final var allItems = new ArrayList<ZkAst.ProgramItem<? extends ZkAst.ProgramItemData>>();
    allItems.addAll(program.useDeclarations());
    allItems.addAll(program.modules());
    allItems.addAll(program.typeAliases());
    allItems.addAll(program.structs());
    allItems.addAll(program.constants());
    allItems.addAll(program.functions());
    return allItems;
  }

  private static void prettyItemVisibility(
      final ZkAst.ItemVisibility visibility, final PrettyBuilder output) {
    if (visibility == ZkAst.ItemVisibility.PUBLIC) {
      output.append("pub ");
    } else if (visibility == ZkAst.ItemVisibility.ONLY_CRATE) {
      output.append("pub(crate) ");
    }
  }

  private static void prettyMetaItem(ZkAst.MetaItem uncheckedItem, final PrettyBuilder output) {
    // Meta item name
    if (uncheckedItem instanceof ZkAst.MetaItem.Name item) {
      output.append(item.path().stream().collect(Collectors.joining("::")));

      // Meta item assign
    } else if (uncheckedItem instanceof ZkAst.MetaItem.Assign item) {
      prettyMetaItem(item.name(), output);
      output.append(" = ");
      prettyExpression(item.value(), output);

      // Meta item call
    } else {
      final ZkAst.MetaItem.Call item = (ZkAst.MetaItem.Call) uncheckedItem;
      prettyMetaItem(item.name(), output);
      output.append("(");
      boolean first = true;
      for (final var argumentItem : item.arguments()) {
        if (!first) {
          output.append(", ");
        }
        first = false;
        prettyMetaItemInner(argumentItem, output);
      }
      output.append(")");
    }
  }

  private static void prettyMetaItemInner(
      ZkAst.MetaItemInner uncheckedItem, final PrettyBuilder output) {
    if (uncheckedItem instanceof ZkAst.MetaItemInner.MetaItem item) {
      prettyMetaItem(item.metaItem(), output);
    } else {
      final ZkAst.MetaItemInner.Expression item = (ZkAst.MetaItemInner.Expression) uncheckedItem;
      prettyExpression(item.expression(), output);
    }
  }

  private static void prettyItem(
      ZkAst.ProgramItem<? extends ZkAst.ProgramItemData> uncheckedItem,
      final PrettyBuilder output) {
    // Attributes
    for (final var attr : uncheckedItem.attributes()) {
      output.append("#[");
      prettyMetaItem(attr.metaItem(), output);
      output.append("]");
      output.append("\n");
    }

    // Visibility
    prettyItemVisibility(uncheckedItem.visibility(), output);

    // Item itself
    prettyItem(uncheckedItem.item(), output);
  }

  private static void prettyItem(ZkAst.ProgramItemData uncheckedItem, final PrettyBuilder output) {
    // Use declarations
    if (uncheckedItem instanceof ZkAst.UseDeclaration item) {
      prettyUseDeclaration(item, output);

      // Type Alias
    } else if (uncheckedItem instanceof ZkAst.TypeAlias item) {
      prettyTypeAlias(item, output);

      // Structs
    } else if (uncheckedItem instanceof ZkAst.StructDefinition item) {
      prettyStructDefinition(item, output);

      // Constants
    } else if (uncheckedItem instanceof ZkAst.ConstantDefinition item) {
      prettyConstantDefinition(item, output);

      // Function
    } else if (uncheckedItem instanceof ZkAst.Function item) {
      prettyFunction(item, output);

      // Module
    } else {
      ZkAst.Module item = (ZkAst.Module) uncheckedItem;
      prettyModule(item, output);
    }
  }

  private static void prettyModule(ZkAst.Module module, final PrettyBuilder output) {
    output.append("mod ").append(module.moduleName()).append(";");
  }

  private static void prettyUseDeclaration(
      final ZkAst.UseDeclaration useDeclaration, final PrettyBuilder output) {
    // Module path
    output.append("use ");
    for (final String modulePathElement : useDeclaration.modulePath()) {
      output.append(modulePathElement);
      output.append("::");
    }

    // Import everything
    if (useDeclaration.moduleElements() == null) {
      output.append("*");

      // Import specific
    } else if (useDeclaration.moduleElements().size() == 1) {
      output.append(useDeclaration.moduleElements().get(0));

      // Import specific, multiple
    } else {
      output.append("{");
      boolean first = true;
      for (final String element : useDeclaration.moduleElements()) {
        if (first) {
          first = false;
        } else {
          output.append(", ");
        }
        output.append(element);
      }
      output.append("}");
    }
    output.append(";");
  }

  private static void prettyStructDefinition(
      final ZkAst.StructDefinition struct, final PrettyBuilder output) {
    output.append("struct ").append(struct.structIdentifier());

    if (!struct.genericTypeParams().isEmpty()) {
      output.append("<");
      prettyList(struct.genericTypeParams(), output, ZkAstPretty::prettyGenericTypeParam);
      output.append(">");
    }

    output.append(" {");
    if (struct.fields().isEmpty()) {
      output.append(" ");
    } else {
      final PrettyBuilder fieldsOutput = output.withIndent();
      for (final ZkAst.StructField field : struct.fields()) {
        fieldsOutput.appendNewline();
        prettyStructField(field, fieldsOutput);
        fieldsOutput.append(",");
      }
      output.appendNewline();
    }
    output.append("}");
  }

  private static void prettyGenericTypeParam(
      final ZkAst.GenericTypeParam genericTypeParam, final PrettyBuilder output) {
    output.append(genericTypeParam.boundName());
    if (genericTypeParam.traitConstraint() != null) {
      output.append(": ").append(genericTypeParam.traitConstraint());
    }
  }

  private static void prettyStructField(final ZkAst.StructField field, final PrettyBuilder output) {
    output.append(field.fieldName()).append(": ");
    prettyType(field.fieldType(), output);
  }

  private static void prettyTypeAlias(final ZkAst.TypeAlias typeAlias, final PrettyBuilder output) {
    output.append("type ").append(typeAlias.aliasIdentifier()).append(" = ");
    prettyType(typeAlias.type(), output);
    output.append(";");
  }

  private static void prettyFunction(final ZkAst.Function function, final PrettyBuilder output) {
    output.append("fn ").append(function.identifier()).append("(");

    prettyList(function.parameters(), output, ZkAstPretty::prettyParameter);

    output.append(")");
    if (!isUnitType(function.returnType())) {
      output.append(" -> ");
      prettyType(function.returnType(), output);
    }
    output.append(" ");
    prettyBlockExpression(function.body(), output);
  }

  private static void prettyParameter(final ZkAst.Parameter parameter, final PrettyBuilder output) {
    if (parameter.isMutable()) {
      output.append("mut ");
    }
    output.append(parameter.identifier()).append(": ");
    prettyType(parameter.type(), output);
  }

  private static void prettyBlockExpression(
      final ZkAst.Expression expression, final PrettyBuilder output) {
    prettyExpression(
        expression instanceof ZkAst.Expression.Block
            ? expression
            : new ZkAst.Expression.Block(List.of(), expression, expression.position()),
        output);
  }

  private static void prettyConstantDefinition(
      final ZkAst.ConstantDefinition constDef, final PrettyBuilder output) {
    output.append("const ").append(constDef.identifier()).append(": ");
    prettyType(constDef.type(), output);
    output.append(" = ");
    prettyExpression(constDef.initExpr(), output);
    output.append(";");
  }

  /**
   * Pretty-prints ZkAst type.
   *
   * @param type Type to pretty-print.
   * @return prettified string version of the given type.
   */
  public static String prettyType(final ZkAst.Type type) {
    final var output = new PrettyBuilder(new StringBuilder(), 0, false);
    prettyType(type, output);
    return output.output().toString();
  }

  private static void prettyType(final ZkAst.Type uncheckedType, final PrettyBuilder output) {
    if (uncheckedType instanceof ZkAst.Type.Tuple type) {
      output.append("(");
      prettyList(type.subtypes(), output, ZkAstPretty::prettyType);
      output.append(")");
    } else if (uncheckedType instanceof ZkAst.Type.Array type) {
      output.append("[");
      prettyType(type.baseType(), output);
      output.append("; ");
      prettyExpression(type.sizeExpr(), output);
      output.append("]");
    } else if (uncheckedType instanceof ZkAst.Type.Slice type) {
      output.append("[");
      prettyType(type.baseType(), output);
      output.append("]");
    } else if (uncheckedType instanceof ZkAst.Type.Named type) {
      prettyPathInType(type.namePath(), output);
    } else {
      final ZkAst.Type.Reference type = (ZkAst.Type.Reference) uncheckedType;
      output.append(type.isMutable() ? "&mut " : "&");
      prettyType(type.baseType(), output);
    }
  }

  private static void prettyExpression(
      final ZkAst.Expression uncheckedExpression, final PrettyBuilder output) {
    if (uncheckedExpression instanceof ZkAst.Expression.Block expression) {

      final var innerOutput = output.withIndent().withWithinExpression(false);
      innerOutput.append("{");

      // Statements
      for (final ZkAst.Statement statement : expression.statements()) {
        innerOutput.appendNewline();
        prettyStatement(statement, innerOutput);
        if (!isCurlyBlock(statement)) {
          innerOutput.append(";");
        }
      }

      // Result expression
      if (!isUnitConstant(expression.returnExpr())) {
        final boolean temporarilyRequiresParens = isCurlyBlock(expression.returnExpr());
        innerOutput.appendNewline();
        if (temporarilyRequiresParens) {
          innerOutput.append("(");
        }
        prettyExpression(expression.returnExpr(), innerOutput);
        if (temporarilyRequiresParens) {
          innerOutput.append(")");
        }
      }
      output.appendNewline().append("}");
    } else if (uncheckedExpression instanceof ZkAst.Expression.Unary expression) {
      if (output.withinExpression()) {
        output.append("(");
      }
      output.append(UNARY_OP_MAP.get(expression.operation()));
      prettyExpression(expression.expr1(), output.withWithinExpression(true));
      if (output.withinExpression()) {
        output.append(")");
      }

    } else if (uncheckedExpression instanceof ZkAst.Expression.Binary expression
        && expression.operation().equals(ZkAst.Binop.INDEX)) {
      prettyExpression(expression.expr1(), output.withWithinExpression(true));
      output.append("[");
      prettyExpression(expression.expr2(), output.withWithinExpression(false));
      output.append("]");

    } else if (uncheckedExpression instanceof ZkAst.Expression.Binary expression) {
      if (output.withinExpression()) {
        output.append("(");
      }
      prettyExpression(expression.expr1(), output.withWithinExpression(true));
      output.append(" ").append(BINARY_OP_MAP.get(expression.operation())).append(" ");
      prettyExpression(expression.expr2(), output.withWithinExpression(true));
      if (output.withinExpression()) {
        output.append(")");
      }

    } else if (uncheckedExpression instanceof ZkAst.Expression.Variable expression) {
      prettyPathInExpression(expression.variablePath(), output);

    } else if (uncheckedExpression instanceof ZkAst.Expression.BoolLiteral expression) {
      output.append(expression.value() ? "true" : "false");

    } else if (uncheckedExpression instanceof ZkAst.Expression.IntLiteral expression) {
      output.append("" + expression.value());
      if (expression.explicitType() != null) {
        prettyType(expression.explicitType(), output);
      }

    } else if (uncheckedExpression instanceof ZkAst.Expression.StringLiteral expression) {
      output.append("\"").append(expression.value()).append("\"");

    } else if (uncheckedExpression instanceof ZkAst.Expression.TupleConstructor expression) {
      output.append("(");
      prettyList(
          expression.exprs(), output.withWithinExpression(false), ZkAstPretty::prettyExpression);
      output.append(")");

    } else if (uncheckedExpression instanceof ZkAst.Expression.Call expression) {
      prettyExpression(expression.functionExpr(), output);
      output.append("(");
      prettyList(
          expression.valueArgumentExprs(),
          output.withWithinExpression(false),
          ZkAstPretty::prettyExpression);
      output.append(")");

    } else if (uncheckedExpression instanceof ZkAst.Expression.MethodCall expression) {
      prettyExpression(expression.objectExpr(), output);
      output.append(".").append(expression.methodIdentifier()).append("(");
      prettyList(
          expression.parameterExprs(),
          output.withWithinExpression(false),
          ZkAstPretty::prettyExpression);
      output.append(")");

    } else if (uncheckedExpression instanceof ZkAst.Expression.MacroInvocation expression) {
      output.append(String.join("::", expression.macroIdentifier()));
      output.append("!(");
      prettyList(
          expression.parameterExprs(),
          output.withWithinExpression(false),
          ZkAstPretty::prettyExpression);
      output.append(")");

    } else if (uncheckedExpression instanceof ZkAst.Expression.StructConstructor expression) {
      prettyPathInExpression(expression.typePath(), output);
      output.append(" { ");
      prettyList(
          expression.fieldInits(),
          output.withWithinExpression(false),
          ZkAstPretty::prettyFieldInit);
      output.append(" }");

    } else if (uncheckedExpression instanceof ZkAst.Expression.If expression) {
      output.append("if ");
      prettyExpression(expression.condExpr(), output.withWithinExpression(false));
      output.append(" ");
      prettyBlockExpression(expression.thenExpr(), output);
      if (!isUnitConstant(expression.elseExpr())) {
        output.append(" else ");
        if (expression.elseExpr() instanceof ZkAst.Expression.If) {
          prettyExpression(expression.elseExpr(), output);
        } else {
          prettyBlockExpression(expression.elseExpr(), output);
        }
      }

    } else if (uncheckedExpression instanceof ZkAst.Expression.For expression) {
      output.append("for ").append(expression.iteratorVariableIdentifier()).append(" in ");
      prettyExpression(expression.iteratorExpr(), output.withWithinExpression(false));
      output.append(" ");
      prettyBlockExpression(expression.body(), output);

    } else if (uncheckedExpression instanceof ZkAst.Expression.FieldAccess expression) {
      prettyExpression(expression.objectExpr(), output.withWithinExpression(true));
      output.append(".").append(expression.fieldIdentifier());

    } else if (uncheckedExpression instanceof ZkAst.Expression.TupleAccess expression) {
      prettyExpression(expression.tupleExpr(), output.withWithinExpression(true));
      output.append(".").append("" + expression.index());

    } else if (uncheckedExpression instanceof ZkAst.Expression.Break expression) {
      output.append("break");

    } else if (uncheckedExpression instanceof ZkAst.Expression.Continue expression) {
      output.append("continue");

    } else if (uncheckedExpression instanceof ZkAst.Expression.ArrayConstructorRepeat expression) {
      output.append("[");
      prettyExpression(expression.repeatExpression(), output.withWithinExpression(true));
      output.append("; ");
      prettyExpression(expression.sizeExpression(), output.withWithinExpression(true));
      output.append("]");

    } else if (uncheckedExpression
        instanceof ZkAst.Expression.ArrayConstructorExplicit expression) {
      output.append("[");
      prettyList(
          expression.elementExpressions(),
          output.withWithinExpression(true),
          ZkAstPretty::prettyExpression);
      output.append("]");

    } else if (uncheckedExpression instanceof ZkAst.Expression.TypeCast expression) {
      if (output.withinExpression()) {
        output.append("(");
      }
      prettyExpression(expression.valueExpr(), output.withWithinExpression(true));
      output.append(" as ");
      prettyType(expression.resultType(), output);
      if (output.withinExpression()) {
        output.append(")");
      }

    } else {
      final ZkAst.Expression.Assign expression = (ZkAst.Expression.Assign) uncheckedExpression;

      if (output.withinExpression()) {
        output.append("(");
      }
      prettyExpression(expression.placeExpr(), output.withWithinExpression(true));
      output.append(" = ");
      prettyExpression(expression.valueExpr(), output.withWithinExpression(true));
      if (output.withinExpression()) {
        output.append(")");
      }
    }
  }

  /**
   * Prettifies a struct field initialization expression. If the expression is a variable with the
   * exact path as the field name, only writes short-form initialization.
   *
   * @param field which is turned to a prettified string.
   * @param output where the prettified string is appended.
   */
  private static void prettyFieldInit(
      final ZkAst.Expression.StructConstructor.Field field, final PrettyBuilder output) {
    if (field.fieldInitExpr() instanceof ZkAst.Expression.Variable varExp
        && varExp.variablePath().path().equals(List.of(field.fieldName()))) {
      output.append(field.fieldName());
    } else {
      output.append(field.fieldName()).append(": ");
      prettyExpression(field.fieldInitExpr(), output);
    }
  }

  private static void prettyStatement(
      final ZkAst.Statement uncheckedStatement, final PrettyBuilder output) {
    if (uncheckedStatement instanceof ZkAst.Statement.Let statement) {
      output.append("let ");
      // Is mut?
      if (statement.isMutable()) {
        output.append("mut ");
      }
      output.append(statement.identifier());

      // Has type declaration?
      if (statement.type() != null) {
        output.append(": ");
        prettyType(statement.type(), output);
      }

      output.append(" = ");
      prettyExpression(statement.initExpr(), output);
    } else if (uncheckedStatement instanceof ZkAst.Statement.Return statement) {
      output.append("return ");
      prettyExpression(statement.resultExpr(), output);
    } else {
      final ZkAst.Statement.Expr statement = (ZkAst.Statement.Expr) uncheckedStatement;
      prettyExpression(statement.expr(), output);
    }
  }

  private static void prettyPathInExpression(ZkAst.Path path, PrettyBuilder output) {
    prettyPath(path, output, true);
  }

  private static void prettyPathInType(ZkAst.Path path, PrettyBuilder output) {
    prettyPath(path, output, false);
  }

  /**
   * Pretty-prints ZkAst path.
   *
   * @param path Path to pretty-print.
   * @param inExpression Whether the expression or type format should be used.
   * @return prettified string version of the given path.
   */
  public static String prettyPath(final ZkAst.Path path, boolean inExpression) {
    final var output = new PrettyBuilder(new StringBuilder(), 0, false);
    prettyPath(path, output, inExpression);
    return output.output().toString();
  }

  private static void prettyPath(ZkAst.Path path, PrettyBuilder output, boolean inExpression) {
    output.append(path.path().stream().collect(Collectors.joining("::")));
    if (!path.genericTypes().isEmpty()) {
      if (inExpression) {
        output.append("::");
      }
      output.append("<");
      prettyList(path.genericTypes(), output.withWithinExpression(false), ZkAstPretty::prettyType);
      output.append(">");
    }
  }

  private static final Map<ZkAst.Unop, String> UNARY_OP_MAP =
      ZkAstConverter.UNARY_OP_MAP.entrySet().stream()
          .collect(Collectors.toUnmodifiableMap(Map.Entry::getValue, Map.Entry::getKey));

  private static final Map<ZkAst.Binop, String> BINARY_OP_MAP =
      ZkAstConverter.BINARY_OP_MAP.entrySet().stream()
          .collect(Collectors.toUnmodifiableMap(Map.Entry::getValue, Map.Entry::getKey));

  // Utility

  private static boolean isCurlyBlock(final ZkAst.Statement uncheckedStatement) {
    if (uncheckedStatement instanceof ZkAst.Statement.Expr statement) {
      return isCurlyBlock(statement.expr());
    }
    return false;
  }

  private static boolean isCurlyBlock(final ZkAst.Expression expr) {
    return expr instanceof ZkAst.Expression.If || expr instanceof ZkAst.Expression.For;
  }

  private static boolean isUnitConstant(final ZkAst.Expression uncheckedExpr) {
    return uncheckedExpr instanceof ZkAst.Expression.TupleConstructor expr
        && expr.exprs().isEmpty();
  }

  private static boolean isUnitType(final ZkAst.Type uncheckedType) {
    return uncheckedType instanceof ZkAst.Type.Tuple type && type.subtypes().isEmpty();
  }

  private static <T> void prettyList(
      final List<T> list,
      final PrettyBuilder output,
      final BiConsumer<T, PrettyBuilder> prettyElem) {
    prettyList(list, output, ", ", prettyElem);
  }

  private static <T> void prettyList(
      final List<T> list,
      final PrettyBuilder output,
      final String separator,
      final BiConsumer<T, PrettyBuilder> prettyElem) {
    int idx = 0;
    for (final T item : list) {
      if (idx++ > 0) {
        output.append(separator);
      }
      prettyElem.accept(item, output);
    }
  }
}
