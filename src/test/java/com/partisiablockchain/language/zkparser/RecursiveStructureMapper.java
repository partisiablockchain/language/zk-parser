package com.partisiablockchain.language.zkparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.lang.reflect.RecordComponent;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

/** Provides utility for replacing the individual objects within the data-structure. */
public final class RecursiveStructureMapper {

  /**
   * Replaces all instances of mappedObjectClass by applying mapping to them, returning a new
   * structure, rather that modifying the old one. Does not support structures with circular
   * references.
   *
   * @param root Root of the data structure.
   * @param mappedObjectClass Class of the desired objects to map.
   * @param mapping Mapping function for transforming the objects.
   * @return Newly created datastructure.
   */
  @SuppressWarnings("unchecked")
  public static <T, S> T map(
      final T root, final Class<S> mappedObjectClass, final Function<S, S> mapping) {
    assert !root.getClass().equals(mappedObjectClass);
    return (T) mapInternal(root, mappedObjectClass, (Function<Object, Object>) mapping);
  }

  private static Object mapInternal(
      final Object obj, final Class<?> mappedObjectClass, final Function<Object, Object> mapping) {

    // Null objects
    if (obj == null) {
      return null;
    }

    final var cls = obj.getClass();

    if (cls.equals(mappedObjectClass)) {
      return mapping.apply(obj);
    }

    // Enums, primitives, Well-known class
    if (cls.isEnum() || cls.isPrimitive() || BASIC_CLASSES.contains(cls)) {
      return obj;
    }

    // Records
    if (cls.isRecord()) {
      final var inputs =
          Arrays.stream(cls.getRecordComponents())
              .map(
                  field -> {
                    try {
                      return field.getAccessor().invoke(obj);
                    } catch (final ReflectiveOperationException e) {
                      throw new RuntimeException("Cannot extract from", e);
                    }
                  })
              .map(elem -> mapInternal(elem, mappedObjectClass, mapping))
              .toArray();
      try {
        final Class<?>[] inputTypes =
            Arrays.stream(cls.getRecordComponents())
                .map(RecordComponent::getType)
                .toArray(Class<?>[]::new);
        return cls.getDeclaredConstructor(inputTypes).newInstance(inputs);
      } catch (final ReflectiveOperationException e) {
        throw new RuntimeException("Constructor not available for " + cls, e);
      }
    }

    // Lists
    if (obj instanceof List<?> objList) {
      return objList.stream().map(elem -> mapInternal(elem, mappedObjectClass, mapping)).toList();
    }

    // Any other class
    throw new RuntimeException("Unknown class, cannot map");
  }

  private static Set<Class<?>> BASIC_CLASSES =
      Set.of(
          Boolean.class,
          Byte.class,
          Character.class,
          Short.class,
          Integer.class,
          Long.class,
          Float.class,
          Double.class,
          BigInteger.class,
          BigDecimal.class,
          String.class);
}
