use std::ops::Range;

fn func() -> Range<u32> {
    Range::<u32> { start: 1, end: 3 }
}
