fn circuit_1(x: (i32, i32, i32)) -> i32 {
    let x01 = x.0 + x.1;
    let x12 = x.1 + x.2;
    x01 + x12
}
