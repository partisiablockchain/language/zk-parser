
const MY_CONST: i32 = 13211;

fn do_something(x: i32) -> i32 {
    x + MY_CONST
}
