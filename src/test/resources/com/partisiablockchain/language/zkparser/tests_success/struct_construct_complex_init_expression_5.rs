struct MyStruct {
    field1: u32,
    field2: u32,
}

fn func(v1: MyStruct) -> MyStruct {
    MyStruct { field1: v1.field1, field2: v1.field2 }
}
