type MyType = i32;

type MyType2 = MyType;

fn identity(x: MyType) -> MyType2 {
    x
}
