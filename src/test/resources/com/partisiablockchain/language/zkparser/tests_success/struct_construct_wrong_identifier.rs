struct MyStruct {
    f1: i32,
}

pub fn construct() -> MyStruct {
    let n1 = 4;
    MyStruct { n1 }
}
