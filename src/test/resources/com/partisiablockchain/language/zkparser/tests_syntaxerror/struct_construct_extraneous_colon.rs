//! assert fail_parse "no viable alternative at input 'letf1=4;MyStruct{f1:}'"

struct MyStruct {
    f1: i32,
}

pub fn construct() -> MyStruct {
    let f1 = 4;
    MyStruct { f1: }
}
