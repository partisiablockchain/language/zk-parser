//! assert fail_parse "missing ';' at 'pub'"
use pbc_zk::{sbi32_input, Sbi32}

pub fn zk_compute(input_id: i32) -> Sbi32 {
    sbi32_input::(input_id)
}
