use pbc_zk::{Sbi32, load_secret_variable};

fn compute() -> Sbi32 {
    load_secret_variable::<>(1)
}
